import React from 'react';
import {
    Header,
    Footer 
} from "../components";

class PageNotFound extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="ty-helper-container no-touch" id="tygh_main_container">
                <Header/>
                <div className="nk-page-404-desktop" style={{ display: "block" }}>
                <div style={{ 
                    color: 'red',
                    flex: 1,
                    textAlign: "center",
                    justifyContent: "center",
                    paddingTop: 100,
                    paddingBottom: 100,
                    fontSize: 18
                    }}>
                    404 PageNotFound
                    <br/>
                    <br/>
                    Coming Soon!
                </div>
                </div>
                <Footer/>
            </div>  
        );
    }
}

export default PageNotFound;