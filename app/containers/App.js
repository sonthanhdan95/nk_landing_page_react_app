import React from 'react';
import * as Containers from '../containers';
import LoadingBox from './../components/Animations/LoadingBox'
import axios from 'axios';
import {
    URL_API_ENDPOINT
} from './../constant/endpoint_constant';
class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            landingPageId: 0,
            landingData: []
        };
    }
     componentWillMount(){
        const self = this;
        var url = this.props.location.pathname.replace(/^\/([^\/]*).*$/, '$1');
        console.log(url);
        axios.get(`${URL_API_ENDPOINT}/api/seo-landingpage/${url}`,{
            headers: {
                'crossDomain': true,
                'Access-Control-Allow-Origin': '*'
            },
            proxy: {
                host: 'localhost',
                port: 3000
            }
        }).then((response) => {
            console.log("kết nối thành công",response);
            const landingPageId = (response.data.ldp_id > 0)? response.data.ldp_id : 404;
            console.log('response',landingPageId);
            self.setState({
                isLoading: true,
                landingPageId: landingPageId
            });

        }).catch(err => {
            throw err;
        });
    }
    render(){
       
        const {landingPageId,isLoading} = this.state;
        console.log('isLoading',isLoading);
        console.log('landingPageId',landingPageId);
        let loadApp = <LoadingBox isShow={true} />;
        if (landingPageId > 0 && landingPageId != 404) {
            loadApp = <Containers.LandingPage landingPageId={landingPageId} />;
        } else if (landingPageId == 404){
            loadApp = <Containers.PageNotFound />;
        }
        return (
            <div className="ty-tygh" id="tygh_container">
            {loadApp}
            </div>
        );
    }

}

export default App;