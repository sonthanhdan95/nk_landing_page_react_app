import React from "react";
import {
    MetaTags,
    Header,
    Floor,
    Footer 
} from "../components";
import {
  URL_API_ENDPOINT
} from './../constant/endpoint_constant';
import axios from 'axios';

class LandingPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      landingData: {},
      landingLayout: [],
      isLoading: false
    };
  }
  componentWillUnmount() {
  
  }
  render() {
    const {landingLayout,landingData} = this.state;
    console.log('landingData ',landingData);
    const ListFloor = landingLayout.map((item,index) =>{
      return <Floor
          key={index}
          floorId={item.floor_id}
          type={item.type}
          position={item.position}
          status={item.status}
          components={item.components}
          />
    });
    return (
      <div className="ty-helper-container no-touch" id="tygh_main_container">
        <MetaTags
          dataProps={landingData}
        />
        <Header/>
        {ListFloor}
        <Footer/>
      </div>
    );
  }
  componentDidMount() {
    const self = this;
    axios.get(`${URL_API_ENDPOINT}/api/detail-landingpage/${this.props.landingPageId}`,{
            headers: {
                'crossDomain': true,
                'Access-Control-Allow-Origin': '*'
            },
            proxy: {
                host: 'localhost',
                port: 3000
            }
        }).then((response) => {
          const { landing_page,layouts } = response.data;
          self.setState({
              landingData   : landing_page,
              landingLayout : layouts,
              isLoading: true
            });
        }).catch(err => {
            throw err;
        });
  }
  componentDidUpdate(){
  
  }
  componentWillUnmount(){
   
  }
}

export default LandingPage;
