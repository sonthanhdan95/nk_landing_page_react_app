import { createLogger } from 'redux-logger';
import * as types from './constant/action_constant'

const logger = createLogger({
    predicate: function(getState,action){
        action.type !== types.FETCH_DATA_USER
    }
});

export default logger;