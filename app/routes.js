import React from 'react';
import { Route} from 'react-router';
import * as Containers from './containers';


export default (
    <Route path='/*' exact component={Containers.App} ></Route>
);