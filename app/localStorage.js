export function saveQueueState(state) {
    try {
      const serializedQueueState = JSON.stringify(state.queueState);
      localStorage.setItem('queueState', serializedQueueState);
    } catch (err) {
      // ignore
    }
  }