import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import _throttle from 'lodash.throttle';
import { routerReducer } from 'react-router-redux';
import rootReducer from './reducers';
import {saveQueueState } from './localStorage';
let middleware = [thunk];
if(process.env.NODE_ENV !== 'production'){
    const logger = require('./logger').default;
    middleware = [...middleware];
}
const persistedData = {

};
const store = createStore(rootReducer,persistedData,applyMiddleware(...middleware));
store.subscribe(_throttle(function(){
    saveQueueState(store.getState());
},1000 * 60 * 5));

export default store;