import React from 'react';
import ReactDOM from 'react-dom';
import {Router,browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import routes from './routes';
import store from './store';

// import './styles/base.sass';
class App extends React.Component {
    render(){
        return(
            <div>
            <Provider store={store}>
                <Router 
                    history={browserHistory}
                    routes={routes}
                    onUpdate={() => window.scrollTo(0, 0)}
                />
            </Provider>
           </div>          
        );
    }
}
ReactDOM.render(<App/>,document.getElementById('app'));

