
export function FORMAT_CURRENCY(nStr) {
    if(nStr !== null)
    {
        nStr += '';
        var words = nStr.split('.');
        var Str = words[0];
     
        var groupSeperate = ".";
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(Str)) {
            Str = Str.replace(rgx, '$1' + groupSeperate + '$2');
        }
        return Str + "đ";
    }
    return "0đ";
}


export function FORMAT_PHONE(nStr){
    if (nStr === undefined || nStr === null || nStr.length !== 10)
    {
        return nStr;
    }

    var first = nStr.slice(0, 4);
    var second = nStr.slice(4, 7);
    var end = nStr.slice(7, 10);
    return first+"."+second+"."+end ;
}