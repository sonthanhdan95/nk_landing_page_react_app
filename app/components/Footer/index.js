import React from 'react';
import './index.sass';

class Footer extends React.Component {
    render(){
        return(
            <div>
            <div className="tygh-content clearfix">
              <div className="container-fluid  content-grid">
                <div className="row-fluid ">
                  <div className="span16 main-content-grid">
                    <div id="breadcrumbs_10" className="nk-breamcrumb" />
                    <div className="ty-mainbox-container clearfix">
                      <div className="ty-mainbox-body">   
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="tygh-footer clearfix" id="tygh_footer">
              <div className="container-fluid">
                <div className="row-fluid">
                  <div className="span16 nk-brand margbt10">
                    <div className="row-fluid ">
                      <div className="span16 container">
                        <div className=" row bg-white flexthis border-bg">
                          <h2>Các thương hiệu lớn</h2>
                          <div className="owl-carousel nk-big-brand owl-loaded owl-drag">
                            <div className="owl-stage-outer">
                              <div className="owl-stage" style={{transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: 1599}}>
                                <div className="owl-item active" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L3NhbXN1bmc="><i className="nk-sprite-desktop icon-brand-samsung" /></span></div>
                                </div>
                                <div className="owl-item active" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L3Nvbnk="><i className="nk-sprite-desktop icon-brand-sony" /></span></div>
                                </div>
                                <div className="owl-item active" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L2xn"><i className="nk-sprite-desktop icon-brand-lg" /></span></div>
                                </div>
                                <div className="owl-item active" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L3Rvc2hpYmE="><i className="nk-sprite-desktop icon-brand-toshiba" /></span></div>
                                </div>
                                <div className="owl-item active" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L2RhaWtpbg=="><i className="nk-sprite-desktop icon-brand-daikin" /></span></div>
                                </div>
                                <div className="owl-item active" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L2FwcGxl"><i className="nk-sprite-desktop icon-brand-apple" /></span></div>
                                </div>
                                <div className="owl-item active" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L3BhbmFzb25pYw=="><i className="nk-sprite-desktop icon-brand-panasonic" /></span></div>
                                </div>
                                <div className="owl-item" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L2Nhbm9u"><i className="nk-sprite-desktop icon-brand-canon" /></span></div>
                                </div>
                                <div className="owl-item" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L2FxdWE="><i className="nk-sprite-desktop icon-brand-aqua" /></span></div>
                                </div>
                                <div className="owl-item" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L2hpdGFjaGk="><i className="nk-sprite-desktop icon-brand-hitachi" /></span></div>
                                </div>
                                <div className="owl-item" style={{width: '125.286px', marginRight: 20}}>
                                  <div className="brand-item"><span className="js_hidden_link" rel="nofollow" data-url="L3RodW9uZy1oaWV1L2VsZWN0cm9sdXg="><i className="nk-sprite-desktop icon-brand-electrolux" /></span></div>
                                </div>
                              </div>
                            </div>
                            <div className="owl-nav"><button type="button" role="presentation" className="owl-prev disabled"><i className="fa fa-angle-left" aria-hidden="true" /></button><button type="button" role="presentation" className="owl-next"><i className="fa fa-angle-right" aria-hidden="true" /></button></div>
                            <div className="owl-dots">
                              <div className="owl-dot active"><span /></div>
                              <div className="owl-dot"><span /></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row-fluid">
                  <div className="span16 nk-service">
                    <div className="row-fluid">
                      <div className="span16 container">
                        <div className=" row flexthis bg-white border-bg">
                          <ul>
                            <li>
                              <span className="js_hidden_link" rel="nofollow" data-url="L2RpY2gtdnUtbGFwLWRhdC12YS1iYW5nLWdpYS12YXQtdHUtbGFwLWRhdC5odG1sP3Bvc2l0aW9uPWljb25sYXBkYXQ=" title="Dịch vụ lắp đặt và bảng giá vật tư lắp đặt tại Siêu thị điện máy Nguyễn Kim">
                                <i className="nk-sprite-desktop icon-lapdat_footer" />
                                <h3>Lắp đặt chuyên nghiệp</h3>
                                <p>Đội ngũ lắp đặt, tư vấn kỹ thuật<br />giàu kinh nghiệm</p>
                              </span>
                            </li>
                            <li>
                              <span className="js_hidden_link" rel="nofollow" data-url="L2todS12dWMtcGh1LXNvbmctZ2lhby1uaGFuLmh0bWw/cG9zaXRpb249aWNvbmdpYW9oYW5n">
                                <i className="nk-sprite-desktop icon-giaohang_footer" />
                                <h3>Giao nhận tiện lợi</h3>
                                <p>Giao nhận trong ngày
                                  <br />nhanh chóng, an toàn
                                </p>
                              </span>
                            </li>
                            <li>
                              <span className="js_hidden_link" rel="nofollow" data-url="L2h1b25nLWRhbi10aGFuaC10b2FuLmh0bWw/cG9zaXRpb249aWNvbnRoYW5odG9hbg==">
                                <i className="nk-sprite-desktop icon-thanhtoan_footer" />
                                <h3>Thanh toán linh hoạt</h3>
                                <p>Phương thức thanh toán
                                  <br />đa dạng, tiện lợi
                                </p>
                              </span>
                            </li>
                            <li>
                              <span className="js_hidden_link" rel="nofollow" data-url="L3F1eS1kaW5oLWRvaS10cmEtc2FuLXBoYW0uaHRtbD9wb3NpdGlvbj1pY29uZG9pdHJh">
                                <i className="nk-sprite-desktop icon-doitra_footer" />
                                <h3>Đổi trả dễ dàng</h3>
                                <p>Đổi sản phẩm bị lỗi kỹ thuật
                                  <br />trong 30 ngày
                                </p>
                              </span>
                            </li>
                            <li>
                              <span className="js_hidden_link" rel="nofollow" data-url="L3RpZXUtY2hpLWJhbi1oYW5nLmh0bWw/cG9zaXRpb249aWNvbmhhdW1haQ==">
                                <i className="nk-sprite-desktop icon-haumai_footer" />
                                <h3>Hậu mãi chu đáo</h3>
                                <p>Tận tình chăm sóc khách hàng
                                  <br />suốt dòng đời sản phẩm
                                </p>
                              </span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row-fluid ">
                  <div className="span16 nk-footer bg-white">
                    <div className="row-fluid ">
                      <div className="span16 container">
                        <div className="row-fluid ">
                          <div className="span8 ">
                            <div id="nk-tong-dai-ho-tro">
                              <div className="icon"><i className="nk-sprite-desktop icon-tuvan_footer" /></div>
                              <div className="text">
                                <h3 className="margbt10">Tổng đài hỗ trợ <span>(8h00 - 22h00)</span></h3>
                                <p className="t1 margbt10">Tổng đài mua hàng:<a href="tel:1900.1267"><span className="red">1900.1267</span></a></p>
                                <p className="t2 margbt10">Giao nhận - Bảo hành:<a href="tel:1900.6612"><span className="green">1900.6612</span></a></p>
                                <p className="t3"><span>Email:</span><a href="mailto:chamsoc@nguyenkim.com">chamsoc@nguyenkim.com</a></p>
                              </div>
                            </div>
                          </div>
                          <div className="span8">
                            <div id="nk-email-newsletter" className="nk-email-newsletter">
                              <div className="icon"><i className="nk-sprite-desktop icon-newsletter_footer" /></div>
                              <div className="text">
                                <h3><strong>Bạn có muốn là người sớm nhất</strong><span>nhận được khuyến mãi hấp dẫn từ Nguyễn Kim?</span></h3>
                                <form action="https://www.nguyenkim.com/" method="post" name="subscribe_form" className="cm-processed-form" id="submitGetEmail" noValidate="novalidate">
                                  <input name="redirect_url" defaultValue="index.php" type="hidden" />
                                  <input name="newsletter_format" defaultValue={2} type="hidden" />
                                  <input name="subscribe_email" id="subscr_email1854" size={20} type="email" className="text-input" placeholder="Nhập địa chỉ email của bạn" required />
                                  <input name="subscribe-man"  title="Đăng ký nhận tin khuyến mãi tại Nguyễn Kim" type="button" defaultValue="Nam" />
                                  <input name="subscribe-woman"  title="Đăng ký nhận tin khuyến mãi tại Nguyễn Kim" type="button" defaultValue="Nữ" />
                                  <input name="dispatch" defaultValue="newsletters.add_subscriber" type="hidden" />
                                  <input id="news_gender" name="gender" defaultValue type="hidden" />
                                  <input id="srcc" name="src" defaultValue="footer-subscribe" type="hidden" />
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row-fluid ">
                          <div className="span16 ">
                            <div id="nk-footer-l" className="clearfix">
                              <div id="nk-footer-menu">
                                <ul>
                                  <li>
                                    <span>Thông tin công ty</span>
                                    <div className="nk-footer-submenu">
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2dpb2ktdGhpZXUtY29uZy10eS5odG1s">Giới thiệu công ty</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L3RpZXUtY2hpLWJhbi1oYW5nLmh0bWw=">Tiêu chí bán hàng</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2RvaS10YWMtY2hpZW4tbHVvYy5odG1s">Đối tác chiến lược</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2NhYy10cnVuZy10YW0tbXVhLXNhbS1uZ3V5ZW4ta2ltLmh0bWw=">Hệ thống trung tâm</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L3R1eWVuLWR1bmcuaHRtbA==">Tuyển dụng</span>
                                    </div>
                                  </li>
                                  <li>
                                    <span>Chính sách</span>
                                    <div className="nk-footer-submenu">
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2RpY2gtdnUtbGFwLWRhdC12YS1iYW5nLWdpYS12YXQtdHUtbGFwLWRhdC5odG1s">Giao nhận - lắp đặt</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2NoaW5oLXNhY2gtZHVuZy10aHUuaHRtbA==">Dùng thử sản phẩm</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2todS12dWMtcGh1LXNvbmctZ2lhby1uaGFuLmh0bWw=">Khu vực giao nhận</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2NoaW5oLXNhY2gtdGhlLWtoYWNoLWhhbmcuaHRtbA==">Chính sách thẻ khách hàng</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2todXllbi1tYWktdHJhLWdvcC10YWktbmd1eWVuLWtpbS5odG1s">Trả góp</span>
                                    </div>
                                  </li>
                                  <li>
                                    <span>Hỗ trợ khách hàng</span>
                                    <div className="nk-footer-submenu">
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2h1b25nLWRhbi1tdWEtaGFuZy5odG1s">Hướng dẫn mua hàng</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2h1b25nLWRhbi10aGFuaC10b2FuLmh0bWw=">Hình thức thanh toán</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L3F1eS1kaW5oLWRvaS10cmEtc2FuLXBoYW0uaHRtbA==">Quy định đổi sản phẩm</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L3RyYS1jdXUtdGhlLXRoYW5oLXZpZW4tbmd1eWVuLWtpbS5odG1s">Tra cứu thẻ thành viên</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2Jhby1oYW5oLmh0bWw=">Bảo Hành &amp; Sửa Chữa</span>
                                    </div>
                                  </li>
                                  <li>
                                    <span>Thông tin</span>
                                    <div className="nk-footer-submenu">
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2xpZW4taGUuaHRtbA==">Liên hệ</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2hvcC10YWMuaHRtbA==">Hợp tác</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2dpYWktdGh1b25nLmh0bWw=">Giải thưởng</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2Jhby1tYXQtdGhvbmctdGluLmh0bWw=">Bảo mật thông tin</span>
                                      <span className="js_hidden_link" rel="nofollow" data-url="L2toYWNoLWhhbmctY2hpYS1zZS10aG9uZy10aW4uaHRtbA==">Chia sẻ thông tin</span>
                                    </div>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div id="nk-footer-r">
                              <div id="nk-b2b">
                                <span>Mua hàng doanh nghiệp</span>
                                <span className="js_hidden_link" rel="nofollow" data-url="L2dpYWktcGhhcC10b2ktdXUtY2hvLWRvYW5oLW5naGllcC12aS5odG1s"><i className="nk-sprite-desktop icon-b2b" /></span>
                              </div>
                              <div id="nk-social">
                                <span>Kết nối với chúng tôi</span>
                                <ul>
                                  <li><a target="_blank" rel="nofollow" href="https://www.facebook.com/Online.NguyenKim"><i className="nk-sprite-desktop icon-facebook" /></a></li>
                                  <li><a target="_blank" rel="nofollow" href="https://www.youtube.com/nguyenkimtv"><i className="nk-sprite-desktop icon-youtube" /></a></li>
                                  <li><a target="_blank" rel="nofollow" href="https://plus.google.com/+nguyenkim/posts"><i className="nk-sprite-desktop icon-google_plus" /></a></li>
                                  <li><a target="_blank" rel="nofollow" href="https://www.instagram.com/nguyenkim.official/"><i className="nk-sprite-desktop icon-instagram" /></a></li>
                                  <li><a target="_blank" rel="nofollow" href="https://www.nguyenkim.com/zalo-page-nguyen-kim-cap-nhat-khuyen-mai-va-uu-dai-moi-nhat.html"><i className="nk-sprite-desktop icon-zalo" /></a></li>
                                </ul>
                              </div>
                            </div>
                            <div style={{clear: 'both', margin: 0, padding: 0}} />
                            <div className="wrap-footer-payship">
                              <ul className="foot_payment_ship">
                                <li className="foot_payment" id="nk-payment">
                                  <h3>Chấp nhận thanh toán</h3>
                                  <ul>
                                    <li><i className="nk-sprite-desktop icon-visa" /></li>
                                    <li><i className="nk-sprite-desktop icon-mastercard" /></li>
                                    <li><i className="nk-sprite-desktop icon-123pay" /></li>
                                    <li><i className="nk-sprite-desktop icon-atm" /></li>
                                    <li><i className="nk-sprite-desktop icon-jcb" /></li>
                                    <li><i className="nk-sprite-desktop icon-vietinbank" /></li>
                                    <li><i className="nk-sprite-desktop icon-tragop" /></li>
                                    <li><i className="nk-sprite-desktop icon-cod" /></li>
                                    <li><i className="nk-sprite-desktop icon-zalopay" /></li>
                                  </ul>
                                </li>
                                <li className="foot_ship_robin">
                                  <div className="foot_ship cttv">
                                    <h3>Các công ty thành viên</h3>
                                    <div className="logo-img-ship">
                                      <ul>
                                        <li><span className="js_hidden_link" rel="nofollow" target="_blank" data-url="aHR0cHM6Ly93d3cucm9iaW5zLnZuLz91dG1fc291cmNlPW5ndXllbmtpbSZ1dG1fbWVkaXVtPXJlZmVycmFsJnV0bV9jb250ZW50PWxvZ28tZm9vdGVy"><i className="nk-sprite-desktop icon-brand-robins" /></span></li>
                                        <li><span className="js_hidden_link" rel="nofollow" target="_blank" data-url="aHR0cDovL3d3dy5iaWdjLnZuLz91dG1fc291cmNlPW5ndXllbmtpbSZ1dG1fbWVkaXVtPXJlZmVycmFsJnV0bV9jb250ZW50PWxvZ28tZm9vdGVy"><i className="nk-sprite-desktop icon-brand-bigc" /></span></li>
                                        <li><i className="nk-sprite-desktop icon-brand-bigcexpress" /></li>
                                        <li><span className="js_hidden_link" rel="nofollow" target="_blank" data-url="aHR0cDovL3d3dy5sYW5jaGkudm4vP3V0bV9zb3VyY2U9bmd1eWVua2ltJnV0bV9tZWRpdW09cmVmZXJyYWwmdXRtX2NvbnRlbnQ9bG9nby1mb290ZXI="><i className="nk-sprite-desktop icon-brand-l" /></span></li>
                                        <li><span className="js_hidden_link" rel="nofollow" target="_blank" data-url="aHR0cDovL2Iycy5jb20udm4vP3V0bV9zb3VyY2U9bmd1eWVua2ltJnV0bV9tZWRpdW09cmVmZXJyYWwmdXRtX2NvbnRlbnQ9bG9nby1mb290ZXI="><i className="nk-sprite-desktop icon-brand-b2s" /></span></li>
                                        <li><i className="nk-sprite-desktop icon-brand-delara" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-lee" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-ff" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-marksspencer" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-fila" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-supersports" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-crocs" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-speedo" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-nb" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-lookkool" /></li>
                                        <li><i className="nk-sprite-desktop icon-brand-komonoya" /></li>
                                      </ul>
                                    </div>
                                  </div>
                                </li>
                                <li className="foot_app">
                                  <div id="nk-download">
                                    <h3>Tải apps Nguyễn Kim</h3>
                                    <p><i className="nk-sprite-desktop icon-barcode" /></p>
                                    <p style={{marginLeft: 2}}><span className="js_hidden_link" rel="nofollow" target="_blank" data-url="aHR0cHM6Ly9wbGF5Lmdvb2dsZS5jb20vc3RvcmUvYXBwcy9kZXRhaWxzP2lkPWNvbS5uZ3V5ZW5raW0uc2hvcHBpbmcmYW1wO3JlZmVycmVyPXV0bV9zb3VyY2UlM0RuZ3V5ZW5raW0lMjZ1dG1fbWVkaXVtJTNEZm9vdGVyJTI2dXRtX2NhbXBhaWduJTNEZ2VuZXJhbA=="><i className="nk-sprite-desktop icon-google-play" /></span><span className="js_hidden_link" rel="nofollow" target="_blank" data-url="aHR0cHM6Ly9pdHVuZXMuYXBwbGUuY29tL2FwcC9hcHBsZS1zdG9yZS9pZDk2NTA5NDU0Mz9tdD04"><i className="nk-sprite-desktop icon-app-store" /></span></p>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row-fluid ">
                  <div className="span16 nk-footer-copyright">
                    <div className="row-fluid ">
                      <div className="span16 container">
                        <div id="nk-footer-copyright-l">
                          <div><span>NGUYENKIM.COM - KÊNH MUA SẮM TRỰC TUYẾN UY TÍN GIÁ TỐT HÀNG ĐẦU VIỆT NAM</span><span>Copyright @ 1999 - 2017 Công ty Cổ phần Thương mại Nguyễn Kim. Giấy chứng nhận ĐKKD số: 0302286281 do Sở KH &amp; ĐT TPHCM cấp lần đầu ngày 22/06/2006. Giấy phép ICP số 01/GP-STTTT do Sở Thông tin &amp; Truyền thông TP.HCM cấp ngày 11/01/2017. Địa chỉ: 63-65-67 Trần Hưng Đạo, P.Cầu Ông Lãnh, Q.1, TP.Hồ Chí Minh. Điện thoại: 1900 1267 - Email: chamsoc@nguyenkim.com</span></div>
                        </div>
                        <div id="nk-footer-copyright-r">
                          <ul>
                            <li><a rel="nofollow" target="_blank" href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=31877"><i className="nk-sprite-desktop icon-register" /></a></li>
                            <li><a rel="nofollow" target="_blank" href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=812"><i className="nk-sprite-desktop icon-check" /></a></li>
                            <li><i className="nk-sprite-desktop icon-copyright" /></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

export default Footer;