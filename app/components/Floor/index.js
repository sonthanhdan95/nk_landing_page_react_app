import React from 'react';
import {
    BannerColumn,
    NavMenu,
    NavTitle,
    BannerHeader,
    ListProduct,
    TextBox,
    CountDownCoupon
} from '../../components';
class Floor extends React.Component {
    constructor(props){
        super(props);
        // console.log('Floor this.props',this.props);
    }
    render(){
       const ListComponent = this.props.components.map((nkComponent,index)=>{
            console.log(nkComponent);
            switch(this.props.type){
                case 'FloorBanner':
                    return <BannerHeader key={index} dataProps={nkComponent}/>
                case 'FloorNavMenu':
                    return <NavMenu key={index} dataProps={nkComponent}/>
                case 'FloorTitle':
                    return <NavTitle key={index} dataProps={nkComponent}/>
                case 'FloorBannerColumn':
                    return <BannerColumn key={index} dataProps={nkComponent}/>
                 case 'FloorListProduct':
                     return <ListProduct key={index} dataProps={nkComponent}/>
                case 'FloorCountDownCoupon':
                    return <CountDownCoupon key={index} dataProps={nkComponent}/>
                case 'FloorTextBox':
                    return <TextBox key={index} dataProps={nkComponent}/>
                default:
                    // console.log('other component',nkComponent);
            }
       }); 
        return(
            <div>
                 {ListComponent}
            </div>
        );
    }
}

export default Floor;
