import React, { PropTypes } from 'react';
import './index.sass';

class CountDownTimer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      days: 0,
      hours: 0,
      min: 0,
      sec: 0,
    }
  }

  componentDidMount() {
    // update every second
    this.interval = setInterval(() => {
      const date = this.calculateCountdown(this.props.date);
      date ? this.setState(date) : this.stop();
    }, 1000);
  }

  componentWillUnmount() {
    this.stop();
  }

  calculateCountdown(endDate) {
    let diff = (Date.parse(new Date(endDate)) - Date.parse(new Date())) / 1000;

    // clear countdown when date is reached
    if (diff <= 0) return false;

    const timeLeft = {
      years: 0,
      days: 0,
      hours: 0,
      min: 0,
      sec: 0,
      millisec: 0,
    };

    // calculate time difference between now and expected date
    if (diff >= (365.25 * 86400)) { // 365.25 * 24 * 60 * 60
      timeLeft.years = Math.floor(diff / (365.25 * 86400));
      diff -= timeLeft.years * 365.25 * 86400;
    }
    if (diff >= 86400) { // 24 * 60 * 60
      timeLeft.days = Math.floor(diff / 86400);
      diff -= timeLeft.days * 86400;
    }
    if (diff >= 3600) { // 60 * 60
      timeLeft.hours = Math.floor(diff / 3600);
      diff -= timeLeft.hours * 3600;
    }
    if (diff >= 60) {
      timeLeft.min = Math.floor(diff / 60);
      diff -= timeLeft.min * 60;
    }
    timeLeft.sec = diff;

    return timeLeft;
  }

  stop() {
    clearInterval(this.interval);
  }

  addLeadingZeros(value) {
    value = String(value);
    while (value.length < 2) {
      value = '0' + value;
    }
    return value;
  }

  render() {
    const countDown = this.state;

    return (

        <div
        className="countdown"
        data-start-time="2018-12-01 00:00:00"
        data-end-time="2018-12-10 00:00:00"
        data-coupon="TESTCOUNTDOWN"
        data-max-usage={50}
        data-login-required="N"
        data-title_notstart="Title start"
        data-title_running="title running"
        data-title_end="title end"
        style={{
            background:
            "url(https://test.nguyenkimonline.com/images/companies/_1/test/BG.png)"
        }}
        >
            <div className="cd-title">title running</div>
            <div className="cd-clock">
                <div className="cl-part day">
                <span className="cp-title">Ngày</span>
                <span className="cp-number">
                    <span className={"digit digit-"+this.addLeadingZeros(countDown.days).substr(0,1)}>{this.addLeadingZeros(countDown.days).substr(0,1)}</span>
                    <span className={"digit digit-"+this.addLeadingZeros(countDown.days).substr(1,1)}>{this.addLeadingZeros(countDown.days).substr(1,1)}</span>
                </span>
                </div>
                <div className="cl-part hour">
                <span className="cp-title">Giờ</span>
                <span className="cp-number">
                    <span className={"digit digit-"+this.addLeadingZeros(countDown.hours).substr(0,1)}>{this.addLeadingZeros(countDown.hours).substr(0,1)}</span>
                    <span className={"digit digit-"+this.addLeadingZeros(countDown.hours).substr(1,1)}>{this.addLeadingZeros(countDown.hours).substr(1,1)}</span>
                </span>
                </div>
                <div className="cl-part minute">
                <span className="cp-title">Phút</span>
                <span className="cp-number">
                    <span className={"digit digit-"+this.addLeadingZeros(countDown.min).substr(0,1)}>{this.addLeadingZeros(countDown.min).substr(0,1)}</span>
                    <span className={"digit digit-"+this.addLeadingZeros(countDown.min).substr(1,1)}>{this.addLeadingZeros(countDown.min).substr(1,1)}</span>
                </span>
                </div>
                <div className="cl-part second">
                <span className="cp-title">Giây</span>
                <span className="cp-number">
                    <span className={"digit digit-"+this.addLeadingZeros(countDown.sec).substr(0,1)}>{this.addLeadingZeros(countDown.sec).substr(0,1)}</span>
                    <span className={"digit digit-"+this.addLeadingZeros(countDown.sec).substr(1,1)}>{this.addLeadingZeros(countDown.sec).substr(1,1)}</span>
                </span>
                </div>
            </div>
            <div className="coupon">
                <span>Nhập mã:</span>
                <span>TESTCOUNTDOWN</span>
            </div>
            <div className="orders" style={{ display: "none" }}>
                <p className="table-title">
                Đã hết <span className="highlight">sô lượt</span> sử dụng, còn 10
                <span className="highlight">0 lượt</span> sử dụng nữa
                </p>
                <table className="table-orders" width="100%" />
            </div>
            <div className="side">
                <div className="hotline">Tổng đài: 1900 1267</div>
            </div>
        
        </div>

      
    );
  }
}

CountDownTimer.propTypes = {
  date: PropTypes.string.isRequired
};

CountDownTimer.defaultProps = {
  date: new Date()
};

export default CountDownTimer;