import React from 'react';
import './index.sass';

class TextBox extends React.Component {
    constructor(props){
        super(props);
        // console.log('TexBox this.props',this.props.dataProps);
    }
    render(){
        const { couponTitle,content } = this.props.dataProps;
        return(
            <div className={"container"}>
                <div className="lp-products">
                    <div className="box" id="d-list-products-16">
                        <div className="title" style={{background: '#59c3ed'}}>
                            <h2 className="head2" style={{padding: 10, color: '#000000', fontSize: 20}}>{couponTitle}</h2>
                        </div>
                        <div className="lp-banner-promotion-key clearfix desktop1items1rows1">
                        </div>
                        <div className="lp-form">
                            <div style={{padding: 24, backgroundColor: 'white', marginBottom: 24, lineHeight: '1.5'}}>
                                <div dangerouslySetInnerHTML={{ __html: content }} />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TextBox;