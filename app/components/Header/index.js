import React from 'react';
import './index.sass';
class Header extends React.Component {
    render(){
        return(
            <div className="container-fluid">
            <div className="row-fluid ">
              <div className="span16 nk-header">
                <div className="row-fluid ">
                  <div className="span16 container">
                    <div className="row-fluid ">
                      <div className="span16 row flexthis">
                        <div className="row-fluid ">
                          <div className="span4 col-md-3">
                            <div id="nk-location" className="nk_tooltip nk_tooltip_ext" data-overlay="true" data-toggle=".popup_location_list">
                              <a className="nk-select-loccation " id="nk-select-loccation">Hồ Chí Minh</a>	
                              <div className="popup_location popup_location_main" style={{display: 'none'}}>
                                <a className="nki-close nk-close-ext bnt-close-message" />
                                <h2>Đây có phải địa chỉ của bạn?</h2>
                                <p>Chọn lại địa điểm để xem đúng giá &amp; khuyến mãi <a className="show_area_popup_desktop">tại đây</a></p>
                              </div>
                              <div className="popup_location popup_location_list" style={{display: 'none'}}>
                                <a className="nki-close nk-close-ext bnt-close-popup" />
                                <h2>Chọn tỉnh thành để xem đúng giá &amp; khuyến mãi:</h2>
                                <div className="nk-search-box">
                                  <input type="text" placeholder="Tìm kiếm tỉnh thành…." className="act_search" />
                                  <button><i className="nki-search" /></button>
                                </div>
                                <div className="nk-location-list flexthis">
                                  <a data-id={18001} data-name="Hồ Chí Minh" data-alias="ho chi minh" data-value={18001} className="item_area">Hồ Chí Minh</a>
                                  <a data-id={18027} data-name="Hà Nội" data-alias="ha noi" data-value={18027} className="item_area">Hà Nội</a>
                                  <a data-id={18010} data-name="An Giang" data-alias="an giang" data-value={18010} className="item_area">An Giang</a>
                                  <a data-id={18012} data-name="Bà Rịa - Vũng Tàu" data-alias="ba ria  vung tau" data-value={18012} className="item_area">Bà Rịa - Vũng Tàu</a>
                                  <a data-id={18014} data-name="Bắc Giang" data-alias="bac giang" data-value={18014} className="item_area">Bắc Giang</a>
                                  <a data-id={18013} data-name="Bắc Kạn" data-alias="bac kan" data-value={18013} className="item_area">Bắc Kạn</a>
                                  <a data-id={18011} data-name="Bạc Liêu" data-alias="bac lieu" data-value={18011} className="item_area">Bạc Liêu</a>
                                  <a data-id={18015} data-name="Bắc Ninh" data-alias="bac ninh" data-value={18015} className="item_area">Bắc Ninh</a>
                                  <a data-id={18006} data-name="Bến Tre" data-alias="ben tre" data-value={18006} className="item_area">Bến Tre</a>
                                  <a data-id={18002} data-name="Bình Dương" data-alias="binh duong" data-value={18002} className="item_area">Bình Dương</a>
                                  <a data-id={18017} data-name="Bình Phước" data-alias="binh phuoc" data-value={18017} className="item_area">Bình Phước</a>
                                  <a data-id={18018} data-name="Bình Thuận" data-alias="binh thuan" data-value={18018} className="item_area">Bình Thuận</a>
                                  <a data-id={18016} data-name="Bình Định" data-alias="binh dinh" data-value={18016} className="item_area">Bình Định</a>
                                  <a data-id={18020} data-name="Cà Mau" data-alias="ca mau" data-value={18020} className="item_area">Cà Mau</a>
                                  <a data-id={18008} data-name="Cần Thơ" data-alias="can tho" data-value={18008} className="item_area">Cần Thơ</a>
                                  <a data-id={18019} data-name="Cao Bằng" data-alias="cao bang" data-value={18019} className="item_area">Cao Bằng</a>
                                  <a data-id={18024} data-name="Gia Lai" data-alias="gia lai" data-value={18024} className="item_area">Gia Lai</a>
                                  <a data-id={18025} data-name="Hà Giang" data-alias="ha giang" data-value={18025} className="item_area">Hà Giang</a>
                                  <a data-id={18026} data-name="Hà Nam" data-alias="ha nam" data-value={18026} className="item_area">Hà Nam</a>
                                  <a data-id={18029} data-name="Hà Tĩnh" data-alias="ha tinh" data-value={18029} className="item_area">Hà Tĩnh</a>
                                  <a data-id={18030} data-name="Hải Dương" data-alias="hai duong" data-value={18030} className="item_area">Hải Dương</a>
                                  <a data-id={18031} data-name="Hải Phòng" data-alias="hai phong" data-value={18031} className="item_area">Hải Phòng</a>
                                  <a data-id={18066} data-name="Hậu Giang" data-alias="hau giang" data-value={18066} className="item_area">Hậu Giang</a>
                                  <a data-id={18032} data-name="Hòa Bình" data-alias="hoa binh" data-value={18032} className="item_area">Hòa Bình</a>
                                  <a data-id={18033} data-name="Hưng Yên" data-alias="hung yen" data-value={18033} className="item_area">Hưng Yên</a>
                                  <a data-id={18034} data-name="Khánh Hòa" data-alias="khanh hoa" data-value={18034} className="item_area">Khánh Hòa</a>
                                  <a data-id={18035} data-name="Kiên Giang" data-alias="kien giang" data-value={18035} className="item_area">Kiên Giang</a>
                                  <a data-id={18036} data-name="Kon Tum" data-alias="kon tum" data-value={18036} className="item_area">Kon Tum</a>
                                  <a data-id={18037} data-name="Lai Châu" data-alias="lai chau" data-value={18037} className="item_area">Lai Châu</a>
                                  <a data-id={18040} data-name="Lâm Đồng" data-alias="lam dong" data-value={18040} className="item_area">Lâm Đồng</a>
                                  <a data-id={18038} data-name="Lạng Sơn" data-alias="lang son" data-value={18038} className="item_area">Lạng Sơn</a>
                                  <a data-id={18039} data-name="Lào Cai" data-alias="lao cai" data-value={18039} className="item_area">Lào Cai</a>
                                  <a data-id={18005} data-name="Long An" data-alias="long an" data-value={18005} className="item_area">Long An</a>
                                  <a data-id={18041} data-name="Nam Định" data-alias="nam dinh" data-value={18041} className="item_area">Nam Định</a>
                                  <a data-id={18042} data-name="Nghệ An" data-alias="nghe an" data-value={18042} className="item_area">Nghệ An</a>
                                  <a data-id={18043} data-name="Ninh Bình" data-alias="ninh binh" data-value={18043} className="item_area">Ninh Bình</a>
                                  <a data-id={18044} data-name="Ninh Thuận" data-alias="ninh thuan" data-value={18044} className="item_area">Ninh Thuận</a>
                                  <a data-id={18045} data-name="Phú Thọ" data-alias="phu tho" data-value={18045} className="item_area">Phú Thọ</a>
                                  <a data-id={18046} data-name="Phú Yên" data-alias="phu yen" data-value={18046} className="item_area">Phú Yên</a>
                                  <a data-id={18047} data-name="Quảng Bình" data-alias="quang binh" data-value={18047} className="item_area">Quảng Bình</a>
                                  <a data-id={18048} data-name="Quảng Nam" data-alias="quang nam" data-value={18048} className="item_area">Quảng Nam</a>
                                  <a data-id={18049} data-name="Quảng Ngãi" data-alias="quang ngai" data-value={18049} className="item_area">Quảng Ngãi</a>
                                  <a data-id={18061} data-name="Quảng Ninh" data-alias="quang ninh" data-value={18061} className="item_area">Quảng Ninh</a>
                                  <a data-id={18009} data-name="Quảng Trị" data-alias="quang tri" data-value={18009} className="item_area">Quảng Trị</a>
                                  <a data-id={18051} data-name="Sóc Trăng" data-alias="soc trang" data-value={18051} className="item_area">Sóc Trăng</a>
                                  <a data-id={18050} data-name="Sơn La" data-alias="son la" data-value={18050} className="item_area">Sơn La</a>
                                  <a data-id={18052} data-name="Tây Ninh" data-alias="tay ninh" data-value={18052} className="item_area">Tây Ninh</a>
                                  <a data-id={18053} data-name="Thái Bình" data-alias="thai binh" data-value={18053} className="item_area">Thái Bình</a>
                                  <a data-id={18054} data-name="Thái Nguyên" data-alias="thai nguyen" data-value={18054} className="item_area">Thái Nguyên</a>
                                  <a data-id={18055} data-name="Thanh Hóa" data-alias="thanh hoa" data-value={18055} className="item_area">Thanh Hóa</a>
                                  <a data-id={18056} data-name="Thừa Thiên Huế" data-alias="thua thien hue" data-value={18056} className="item_area">Thừa Thiên Huế</a>
                                  <a data-id={18007} data-name="Tiền Giang" data-alias="tien giang" data-value={18007} className="item_area">Tiền Giang</a>
                                  <a data-id={18057} data-name="Trà Vinh" data-alias="tra vinh" data-value={18057} className="item_area">Trà Vinh</a>
                                  <a data-id={18058} data-name="Tuyên Quang" data-alias="tuyen quang" data-value={18058} className="item_area">Tuyên Quang</a>
                                  <a data-id={18003} data-name="Vĩnh Long" data-alias="vinh long" data-value={18003} className="item_area">Vĩnh Long</a>
                                  <a data-id={18059} data-name="Vĩnh Phúc" data-alias="vinh phuc" data-value={18059} className="item_area">Vĩnh Phúc</a>
                                  <a data-id={18060} data-name="Yên Bái" data-alias="yen bai" data-value={18060} className="item_area">Yên Bái</a>
                                  <a data-id={18021} data-name="Đà Nẵng" data-alias="da nang" data-value={18021} className="item_area">Đà Nẵng</a>
                                  <a data-id={18022} data-name="Đắk Lắk" data-alias="dak lak" data-value={18022} className="item_area">Đắk Lắk</a>
                                  <a data-id={18068} data-name="Đắk Nông" data-alias="dak nong" data-value={18068} className="item_area">Đắk Nông</a>
                                  <a data-id={18067} data-name="Điện Biên" data-alias="dien bien" data-value={18067} className="item_area">Điện Biên</a>
                                  <a data-id={18004} data-name="Đồng Nai" data-alias="dong nai" data-value={18004} className="item_area">Đồng Nai</a>
                                  <a data-id={18023} data-name="Đồng Tháp" data-alias="dong thap" data-value={18023} className="item_area">Đồng Tháp</a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="span12 col-md-9">
                            <div id="nk-holine">
                              <ul>
                                <li>Chăm sóc khách hàng: <a href="tel:19006612">1900.6612</a></li>
                                <li className="nk_tooltip" data-toggle=".nk-download-app-content" data-overlay="true">
                                  <a href="javascript:void(0)">Tải App nhận ngay ưu đãi</a>
                                  <div className="nk-download-app-content">
                                    <div className="icon-up" />
                                    <div className="ty-dropdown-box">
                                      <p><i className="nk-sprite-desktop icon-barcode-new" /></p>
                                      <p><a target="_blank" href="https://play.google.com/store/apps/details?id=com.nguyenkim.shopping&referrer=utm_source%3Dnguyenkim%26utm_medium%3Dfooter%26utm_campaign%3Dgeneral"><i className="nk-sprite-desktop icon-google-play-new" /></a><a target="_blank" href="https://itunes.apple.com/app/apple-store/id965094543?mt=8"><i className="nk-sprite-desktop icon-app-store-new" /></a></p>
                                    </div>
                                  </div>
                                </li>
                                <li><a href="bao-hanh.html">Bảo hành &amp; Sửa chữa</a></li>
                                <li className="kiem-tra-don-hang-menu nk_tooltip" data-toggle=".content-kiem-tra-don-hang" data-overlay="true">
                                  <p >Kiểm tra đơn hàng</p>
                                  <div className="content-kiem-tra-don-hang">
                                    <div className="icon-up" />
                                    <div className="sp-item follow-orders-item ty-dropdown-box">
                                      <div id="dropdown_orders_" className="sp-item-popup cm-popup-box ty-dropdown-box__content hidden">
                                        <div id="track_orders_block_9945" className="sp-item-sub-popup acount-func-form">
                                          <form id="form_orders_track_request_9945" action="https://www.nguyenkim.com/index.php?dispatch=+" method="get" className="cm-ajax cm-ajax-full-render cm-processed-form" name="track_order_quick">
                                            <input name="dispatch" defaultValue="orders.track_request" type="hidden" />
                                            <input type="hidden" name="result_ids" defaultValue="track_orders_block_*" />
                                            <input type="hidden" name="return_url" defaultValue="index.php?gclid=Cj0KCQiArenfBRCoARIsAFc1FqecizpxGU1_Hd_TCnO_yO73OXy9P93XbfeqSWg29p3SQXu0zWXZJPMaAgPmEALw_wcB&dispatch=nk_landing_page.view&landing_page_id=1420" />
                                            <p className="title-form">Tra cứu đơn hàng</p>
                                            <div className="field">
                                              <i className="fa fa-file-text-o" aria-hidden="true" />
                                              <input type="text" id="order_id" name="track_data[order_id]" placeholder="Mã đơn hàng" className="text-input text-input-check-required" />
                                            </div>
                                            <div className="field">
                                              <i className="fa fa-mobile" aria-hidden="true" />
                                              <input id="order_phone" type="text" size={30} name="track_data[phone]" maxLength={32} placeholder="Số điện thoại" className="text-input text-input-check-required" defaultValue />
                                            </div>
                                            <div className="field-error">
                                              <span id="form_error_message_follow_orders" className="form-error-message red hidden" />
                                            </div>
                                            <div className="btn-section">
                                              <button title="Tìm" className="btn-register" type="submit">Tìm</button>
                                            </div>
                                            <input type="hidden" name="token" defaultValue />
                                          </form>
                                          {/*track_orders_block_9945*/}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li id="login_form" className="nk_tooltip" data-toggle=".nk-login-content" data-overlay="overlay">
                                  <a href="#"><i className="fa fa-user" />Đăng nhập</a>
                                  <div className="nk-login-content">
                                    <div className="icon-up" />
                                    <p className="nk-login-content-body"><a href="javascript:void(0)" className="nk-text-register" >Đăng ký</a><a href="javascript:void(0)" className="nk-text-login" >Đăng nhập</a>        <a href="#"  data-idp="facebook" className="cm-login-provider nk-text-facebook"><i className="fa fa-facebook" /><span>Đăng nhập bằng Facebook</span></a>  </p>
                                  </div>
                                </li>
                              </ul>
                            </div>
                            <div className="wrap-nk-login-form">
                              <div className="nk-bg-popup" />
                              <div className="nk-box-popup nk-box-fix nk-form-register" style={{top: '20px !important'}}>
                                <span className="nk-close-popup"  />        
                                <div className="nk-box-popup-content">
                                  <ul>
                                    <li className="tab-dang-nhap"><a id="nk-form-login" >Đăng nhập</a></li>
                                    <li className="tab-tao-tai-khoan"><a id="nk-form-account" >Tạo tài khoản</a></li>
                                  </ul>
                                  <div className="nk-tab-content">
                                    <div className="nk-form-login nk-form-item">
                                      <form name="login_form" action="https://www.nguyenkim.com/" method="post" className="cm-processed-form">
                                        <input type="hidden" name="security_hash" defaultValue="a156f84a2905dda7cb15e4bace212b6d" />                        <input type="hidden" name="redirect_url" defaultValue="https://www.nguyenkim.com/tivi-lg-oled-uu-dai-khung.html?gclid=Cj0KCQiArenfBRCoARIsAFc1FqecizpxGU1_Hd_TCnO_yO73OXy9P93XbfeqSWg29p3SQXu0zWXZJPMaAgPmEALw_wcB" />                        <input type="hidden" name="dispatch" defaultValue="auth.login" />                        <input type="hidden" name="remember_me" defaultValue={1} />                        <input type="submit" name="sub_login" id="sub_login" style={{display: 'none'}} defaultValue="Đăng nhập" />                        
                                        <div className="nk-form-group">                            <label htmlFor="user_login">Email</label>                            <input className="nk-input-form" type="email" id="user_login" name="user_login" title="vui lòng nhập email" required />                      </div>
                                        <div className="nk-form-group">                            <label>Mật khẩu</label>                            <input className="nk-input-form" type="password" id="user_password" name="password" title="vui lòng nhập mật khẩu" required />                      </div>
                                        <div className="nk-form-group">
                                          <p className="nk-form-margin nk-text-forget-pass">Quên mật khẩu nhấn vào                                 <a href="#" >đây</a>                          </p>
                                          <p className="nk-form-margin">                                <a href="javascript:void(0)"  className="nk-text-login">Đăng nhập</a>                                <a className="nk-text-facebook facebook-link cm-login-provider" data-idp="facebook" ><i className="fa fa-facebook" /><span>Đăng nhập bằng Facebook</span></a>                          </p>
                                        </div>
                                      </form>
                                    </div>
                                    <div className="nk-form-create-acc nk-form-item ">
                                      <form action="https://www.nguyenkim.com/" method="post" name="registry_form" className="cm-processed-form">
                                        <input type="hidden" name="dispatch" defaultValue="profiles.update" />                        <input type="hidden" name="redirect_url" defaultValue="https://www.nguyenkim.com/tivi-lg-oled-uu-dai-khung.html" />                        <input type="hidden" name="src" defaultValue="dang-ky-tai-khoan-destop" />                        <input type="hidden" name="security_hash" defaultValue="a156f84a2905dda7cb15e4bace212b6d" />                        <input type="submit" style={{display: 'none'}} name="submit_registry" id="submit_registry" />                        
                                        <div className="nk-form-group">                            <label htmlFor="cfirstname" className="cm-required">Họ và tên</label>                            <input className="nk-input-form" id="cfirstname" type="text" name="user_data[firstname]" title="vui lòng nhập họ tên" required />                      </div>
                                        <div className="nk-form-group">
                                          <label htmlFor="cphone" className="cm-phone cm-required">Số điện thoại</label>
                                          <input className="nk-input-form" id="cphone" type="text" name="user_data[phone]" title="vui lòng nhập số điện thoại" required />                        
                                        </div>
                                        <div className="nk-form-group">
                                          <p className="nk-form-margin">                                <input className="register-radio-gender nk-input-radio"  id="male_n" type="radio" name="user_data[fields][44]" defaultValue={2} defaultChecked="checked" />                                <label htmlFor="male_n"><span />Anh</label>                                <input className="register-radio-gender nk-input-radio" id="female_n" type="radio" name="user_data[fields][44]" defaultValue={4} />                                <label htmlFor="female_n"><span />Chị</label>                                <input name="gender" type="hidden" id="gender_n" defaultValue="M" />                          </p>
                                        </div>
                                        <div className="nk-form-group">                            <label htmlFor="cemail" className="cm-email cm-required">Email</label>                            <input className="nk-input-form" id="cemail" type="email" name="user_data[email]" title="vui lòng nhập email" required />                      </div>
                                        <div className="nk-form-group">                            <label htmlFor="cpassword1" className="cm-required cm-password">Mật khẩu</label>                            <input className="nk-input-form" id="cpassword1" type="password" name="user_data[password1]" title="vui lòng nhập mật khẩu" required aria-autocomplete="list" />
                                        </div>
                                        <div className="nk-form-group">                            <label htmlFor="cpassword2" className="cm-required cm-password">Xác nhận Mật khẩu</label>                            <input className="nk-input-form" id="cpassword2" type="password" name="user_data[password2]" title="Mật khẩu là không giống nhau" required />
                                        </div>
                                        <div className="nk-form-group">
                                          <p className="nk-form-margin"><a href="javascript:void(0)"  className="nk-btn-click">Tạo tài khoản</a></p>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="wrap-nk-form-forget-pass">
                              <div className="nk-bg-popup" />
                              <div className="nk-box-popup nk-box-fix nk-form-forget-pass">
                                <span className="nk-close-popup"  />        
                                <div className="nk-box-popup-content">
                                  <ul>
                                    <li className="active"><a id="nk-form-forget">Quên mật khẩu</a></li>
                                  </ul>
                                  <div className="nk-tab-content">
                                    <form name="recoverfrm" action="https://www.nguyenkim.com" method="post" className="cm-ajax cm-ajax-full-render cm-processed-form">
                                      <input type="hidden" name="security_hash" defaultValue="a156f84a2905dda7cb15e4bace212b6d" />                    <input type="hidden" name="return_url" defaultValue="https://www.nguyenkim.com/tivi-lg-oled-uu-dai-khung.html?gclid=Cj0KCQiArenfBRCoARIsAFc1FqecizpxGU1_Hd_TCnO_yO73OXy9P93XbfeqSWg29p3SQXu0zWXZJPMaAgPmEALw_wcB" />                    
                                      <p>Vui lòng gửi mail. Chúng tôi sẽ gửi link khởi tạo mật khẩu mới qua email của bạn.</p>
                                      <div className="nk-form-group">
                                        <div className="left">                            <label htmlFor="user_email">Email</label>                            <input className="nk-input-form" type="email" name="user_email" id="user_email" required />                      </div>
                                        <div className="right">                            <input className="nk-btn-click" name="dispatch[auth.recover_password]" defaultValue="Gửi" type="submit" />                      </div>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row-fluid ">
                      <div className="span16 row flexthis">
                        <div className="row-fluid ">
                          <div className="span4 ">
                            <div id="nk-logo"><a href="/"><img src="https://cdn.nguyenkimmall.com/images/companies/_1/html/2017/T11/homepage/Logo_NK.svg" title="Siêu thị điện máy nguyễn kim – Trung tâm mua sắm Sài Gòn Nguyễn Kim - Kênh mua sắm trực tuyến uy tín giá rẻ hàng đầu Việt Nam - Top 500 nhà bán lẻ hàng đầu châu Á - Sự lựa chọn mua hàng thông minh của bạn, luôn có những chương trình khuyến mãi giảm giá ưu đãi cực hấp dẫn." alt="Siêu thị điện máy nguyễn kim – Trung tâm mua sắm Sài Gòn Nguyễn Kim - Kênh mua sắm trực tuyến uy tín giá rẻ hàng đầu Việt Nam - Top 500 nhà bán lẻ hàng đầu châu Á - Sự lựa chọn mua hàng thông minh của bạn, luôn có những chương trình khuyến mãi giảm giá ưu đãi cực hấp dẫn." /></a></div>
                          </div>
                          <div className="span12 nk-nav-right">
                            <div className="row-fluid ">
                              <div className="span8 ">
                                <div id="nk-search">
                                  <form action="https://www.nguyenkim.com/tim-kiem.html" name="search_form" method="get" className="cm-processed-form">
                                    <input type="hidden" id="cid" name="cid" defaultValue />      
                                    <div className="nk-search-danh-muc">
                                      <h3>Tất cả danh mục</h3>
                                      <ul>
                                        <li><a href="javascript:void(0)" data-cateid={0}>Tất cả danh mục</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={338}>Điện tử</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={339}>Điện lạnh</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={340}>Điện thoại - Máy tính bảng</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={341}>Laptop - Vi tính</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={343}>Máy ảnh - Quay phim</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={2894}>Phụ kiện - Thiết bị số</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={2434}>Thiết bị văn phòng</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={345}>Nhà bếp</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={344}>Gia dụng</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={347}>Sức khỏe làm đẹp</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={1080}>Công cụ - Dụng cụ</a></li>
                                        <li><a href="javascript:void(0)" data-cateid={1168}>Bách hóa</a></li>
                                      </ul>
                                    </div>
                                    <div className="nk-search-box">
                                      <input type="text" name="q" defaultValue id="search_input" placeholder="Bạn cần tìm gì hôm nay ?" autoComplete="off" />
                                      <button><i className="fa fa-search" /></button>
                                      <div className="nk-search-hint">
                                        <div className="search-result" style={{display: 'none'}}>
                                          <ul className="nk-search-cate">
                                            <li />
                                          </ul>
                                          <ul className="nk-search-product-item">
                                            <li />
                                          </ul>
                                        </div>
                                      </div>
                                      <input type="hidden" name="subcats" defaultValue="Y" />
                                      <input type="hidden" name="pcode_from_q" defaultValue="Y" />
                                      <input type="hidden" name="pshort" defaultValue="Y" />
                                      <input type="hidden" name="pfull" defaultValue="Y" />
                                      <input type="hidden" name="pname" defaultValue="Y" />
                                      <input type="hidden" name="pkeywords" defaultValue="Y" />
                                      <input type="hidden" name="search_performed" defaultValue="Y" />
                                    </div>
                                  </form>
                                </div>
                              </div>
                              <div className="span8 ">
                                <div id="nk-cart">
                                  <ul>
                                    <li className="cart-info-box">
                                      <a href="javascript:void(0)">
                                        <div className="icon"><i className="nk-sprite-desktop icon-cart" /></div>
                                        <p>Giỏ hàng</p>
                                      </a>
                                      <div className="nk-abandoned-cart" />
                                      <div className="nk-cart-content" />
                                    </li>
                                    <li className="nk_tooltip" data-toggle=".nk-hotline-content" data-overlay="true">
                                      <a href="tel:19006612">
                                        <div className="icon"><i className="nk-sprite-desktop icon-hotline" /></div>
                                        <p><span>1900.1267</span><span>Hotline mua hàng</span></p>
                                      </a>
                                      <div className="nk-hotline-content">
                                        <div className="icon-up" />
                                        <p><span>Tổng đài mua hàng</span><a href="tel:1900.1267"><span className="color1">1900.1267</span></a></p>
                                        <p><span>Chăm sóc khách hàng</span><a href="tel:1900.6612"><span className="color2">1900.6612</span></a></p>
                                        <p><span>Thời gian hoạt động</span><span className="color3">8h00 - 22h00</span></p>
                                      </div>
                                    </li>
                                    <li className="nk_tooltip" data-toggle=".nk-ttms-content" data-overlay="true">
                                      <a href="https://www.nguyenkim.com/cac-trung-tam-mua-sam-nguyen-kim.html">
                                        <div className="icon"><i className="nk-sprite-desktop icon-ttms" /></div>
                                        <p><span>Xem 64 TTMS</span><span>Mở cửa: 8:00 - 22:00</span></p>
                                      </a>
                                      <div className="nk-ttms-content">
                                        <div className="icon-up" />
                                        <div className="bg">
                                          <div className="head">
                                            <p className="title">Có <span />TTMS Nguyễn Kim gần vị trí của bạn</p>
                                          </div>
                                          <div className="middle" />
                                          <div className="footer">
                                            <p><a href="https://www.nguyenkim.com/cac-trung-tam-mua-sam-nguyen-kim.html">Xem chi tiết</a></p>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row-fluid ">
              <div className="span16 nk-menu bg-white clearfix">
                <div className="row-fluid ">
                  <div className="span16 container">
                    <div className="row-fluid ">
                      <div className="span16 row">
                        <div className="row-fluid ">
                          <div className="span4 wrap-grid-menu">
                            <div id="nk-danh-muc-san-pham-left">
                              <h3><i><span /><span /><span /></i><b>Danh mục sản phẩm</b></h3>
                              <ul>
                                <li className>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-new" /></div>
                                    <p><a href="javascript:void(0)">Sắp xếp theo phòng nhà bạn</a></p>
                                  </div>
                                  <div className="sub-menu sap-xep-theo-phong-ban" style={{display: 'none'}}>
                                    <div className=" menu-sap-xep children_sort" style={{display: 'inline-block'}}>
                                      <div className="item row2 bg-white">
                                        <div className="links">
                                          <h3>
                                            Phòng khách
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" data-url="L3RpdmktbWFuLWhpbmgtbGNkLw==">
                                                  Tivi
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/dan-may-nghe-nhac/">                                            Dàn máy nghe nhạc                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh/">                                            Máy lạnh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh/">                                            Tủ lạnh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/quat-dien-quat-may/">                                            Quạt                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            Phòng giải trí
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-man-hinh-lcd/">                                            Tivi                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong/">                                            Điện thoại                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-xach-tay/">                                            Laptop                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dan-may-nghe-nhac/">                                            Dàn máy nghe nhạc                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang/">                                            Máy tính bảng                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/playstation/">                                            Playstation                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-gray">
                                        <div className="links">
                                          <h3>
                                            Phòng ngủ
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh/">                                            Máy lạnh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-loc-khong-khi/">                                            Máy lọc không khí                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/nem/">                                            Nệm                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/bo-dra-trai-giuong-va-ao-goi/">                                            Drap                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/goi/">                                            Gối, áo gối                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            Phòng làm việc
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong/">                                            Điện thoại                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-xach-tay/">                                            Laptop                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang/">                                            Máy tính bảng                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-de-ban/">                                            Máy tính để bàn                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-in-van-phong/">                                            Máy in                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/may-scan/">                                            Máy scan                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L25oYS1iZXAv">Nhà bếp
                                              <span className=" nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh/">                                            Tủ lạnh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/noi-com-dien/">                                            Nồi cơm điện                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/bep-gas/">                                            Bếp gas                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/bep-dien/">                                            Bếp điện từ                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/lo-vi-song-microwave/">                                            Lò vi sóng                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-xay-sinh-to/">                                            Máy xay sinh tố                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-nuoc-nong-lanh/">                                            Máy nước nóng lạnh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-rua-chen/">                                            Máy rửa chén                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/bo-noi-nau-an/">                                            Xoong, Nồi                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/chao-chong-dinh/">                                            Chảo chống dính                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-gray">
                                        <div className="links">
                                          <h3>
                                            Phòng tắm
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat/">                                            Máy giặt                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-nuoc-nong/">                                            Máy nước nóng                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-say-quan-ao/">                                            Máy sấy quần áo                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-say-toc/">                                            Máy sấy tóc, tạo kiểu tóc                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/thung-rac/">                                            Vật dụng vệ sinh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            Sân vườn
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-khoan/">                                            Máy khoan                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-phun-xit-rua/">                                            Máy phun xịt rửa                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-bom-nuoc/">                                            Máy bơm nước                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-phat-dien/">                                            Máy phát điện                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li className>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-maylanh" /></div>
                                    <p><a href="/may-lanh/">Máy lạnh</a><a href="/may-loc-khong-khi/">Máy lọc không khí</a></p>
                                  </div>
                                  <div className="sub-menu may-lanh" style={{display: 'none'}}>
                                    <div className=" menu-maylanh children_sort" style={{display: 'inline-block'}}>
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/may-lanh/">Máy lạnh
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh-daikin/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Daikin                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh-lg/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  LG                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh-toshiba/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Toshiba                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh-sharp/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sharp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh-panasonic/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Panasonic                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh-hitachi/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Hitachi                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh-mitsubishi/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Mitsubishi Electric                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh-reetech/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Reetech                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh/?subcats=Y&features_hash=99999-2">                                            Máy lạnh trả góp 0%                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh/?subcats=Y&features_hash=71-5820">                                            Máy lạnh Inverter                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh/?subcats=Y&features_hash=69-5406">                                            Máy lạnh 1 chiều                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-lanh/?subcats=Y&features_hash=69-7157">                                            Máy lạnh 2 chiều                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L21heS1sb2Mta2hvbmcta2hpLw==">Máy lọc không khí
                                              <span className=" nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-loc-khong-khi-hitachi/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Hitachi                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-loc-khong-khi-sharp/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sharp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-loc-khong-khi-coway/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Coway                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L21heS1odXQtYW0tdGFvLWFtLw==">Máy hút ẩm
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-hut-am-stadler-form/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Stadler Form                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-white">
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L21heS1odXQta2hvaS1raHUtbXVpLw==">Máy hút khói
                                              <span className=" nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-hut-khoi-faber/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Faber                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-hut-khoi-torino/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Torino                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-hut-khoi-electrolux/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Electrolux                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L21heS1zdW9pLWRlbi1zdW9pLw==">Máy sưởi - Đèn sưởi
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-suoi-den-suoi-saiko/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Saiko                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-suoi-den-suoi-steba/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Steba                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-suoi-den-suoi-fakir/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Fakir                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-phone" /></div>
                                    <p><a href="/dien-thoai-di-dong/">Điện thoại </a><a href="/may-tinh-bang/">Tablet</a><a href="/phu-kien-dien-thoai/">Phụ kiện</a></p>
                                  </div>
                                  <div className="sub-menu dien-thoai" style={{display: 'none'}}>
                                    <div className=" menu-dienthoai children_sort">
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/dien-thoai-di-dong/">Điện thoại
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong-apple-iphone/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  iPhone                                                <span className="nki-sticker-hot nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong-samsung/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Samsung                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong-oppo/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  OPPO                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/dien-thoai-di-dong-nokia/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Nokia                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong-sony/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sony                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-vivo/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Vivo                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong-huawei/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Huawei                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong/?subcats=Y&features_hash=99999-2">                                            Điện thoại trả góp 0%                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong/?features_hash=172-36722-35181-71028/">                                            Điện thoại 2 sim 2 sóng                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/dien-thoai-di-dong/?features_hash=365-97229">                                            Màn hình tràn viền                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/dien-thoai-di-dong/?features_hash=255-35633-94179-50764-80110-95173-96289-97137-91531-94355-94343-79384-76460-78114">                                            Chụp ảnh selfie                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dien-thoai-di-dong/?features_hash=23-80164-95399-91829-97139-94181-96949-69312-91789-90763-93559-79714">                                            Camera kép                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/may-tinh-bang/">Tablet
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/may-tinh-bang-apple-ipad/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  iPad                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang-samsung/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Samsung                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang-lenovo/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Lenovo                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang-huawei/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Huawei                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang-acer/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Acer                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang/?subcats=Y&features_hash=99999-2">                                            Tablet trả góp 0%                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang/?features_hash=163-44684-9405-36190-46039-9376">                                            Màn hình lớn                                                 <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-bang/?features_hash=49-51316-16092">                                            Bộ nhớ khủng                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/may-tinh-bang/?features_hash=34-0-4999999-VND  Màn h">                                            Máy tính bảng giá rẻ                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/may-tinh-bang/?features_hash=34-10000001-1000000000-VND">                                            Máy tính bảng cao cấp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/phu-kien-dien-thoai/">Phụ kiện
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="https://www.nguyenkim.com/dong-ho-thong-minh-apple/">                                            Apple Watch                                                <span className="nki-sticker-new nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="https://www.nguyenkim.com/tai-nghe-apple-airpods-itsmmef2za-a.html">                                            AirPods                                                <span className="nki-sticker-hot nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3RhaS1uZ2hlLWRpZW4tdGhvYWkv">
                                                  Tai nghe có dây
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3RhaS1uZ2hlLWJsdWV0b290aC8=">
                                                  Tai nghe Bluetooth
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2Jhby1kYS1vcC1sdW5nLWRpZW4tdGhvYWkv">
                                                  Bao da, Ốp lưng điện thoại
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2Jhby1kYS1tYXktdGluaC1iYW5nLw==">
                                                  Bao da Máy tính bảng
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/pin-sac-du-phong/">                                            Pin sạc dự phòng                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/day-sac-cap-dien-thoai/">                                            Dây sạc, cáp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/gay-chup-hinh-dien-thoai-di-dong/">                                            Gậy chụp hình                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/mieng-dan-dien-thoai/">                                            Miếng dán màn hình                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/cu-sac-dtdd/">                                            Củ sạc                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/dien-thoai-ban/">Điện thoại bàn
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2RpZW4tdGhvYWktYmFuLz9zdWJjYXRzPVkmZmVhdHVyZXNfaGFzaD0zMy01NzQz">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Panasonic
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2RpZW4tdGhvYWktYmFuLz9zdWJjYXRzPVkmZmVhdHVyZXNfaGFzaD0zMy0xMzQwOQ==">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Nippon
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2RpZW4tdGhvYWktYmFuLz9zdWJjYXRzPVkmZmVhdHVyZXNfaGFzaD0zMy00Mzk4OQ==">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Uniden
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2RpZW4tdGhvYWktdm8tdHV5ZW4v">
                                                  Điện thoại không dây
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2RpZW4tdGhvYWktaHV1LXR1eWVuLw==">
                                                  Điện thoại có dây
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-tivi" /></div>
                                    <p><a href="/tivi-man-hinh-lcd/">Tivi</a><a href="/amply-va-loa/">Loa</a><a href="/dan-may-nghe-nhac/">Âm thanh</a></p>
                                  </div>
                                  <div className="sub-menu tivi-loa-amthanh" style={{display: 'none'}}>
                                    <div className=" menu-tivi children_sort">
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="https://www.nguyenkim.com/tivi-man-hinh-lcd/">Tivi
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-lcd-sony/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sony                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/tivi-lcd-samsung/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Samsung                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-lcd-lg/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  LG                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-lcd-toshiba/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Toshiba                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-lcd-sharp/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sharp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-lcd-panasonic/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Panasonic                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-lcd-tcl/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  TCL                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-man-hinh-lcd/?subcats=Y&features_hash=99999-2">                                            Tivi trả góp 0%                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-man-hinh-lcd/?subcats=Y&features_hash=34-0-4999999-VND">                                            Tivi dưới 5 triệu                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-man-hinh-lcd/?subcats=Y&features_hash=76-5857-6493-8-10-12">                                            Tivi 50 - 55 inch                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tivi-man-hinh-lcd/?features_hash=34-100000001-10000000000-VND">                                            Tivi cao cấp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2dpYS10cmVvLXRpdmkv">
                                                  Giá treo tivi
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/amply-va-loa/">Loa - Âm Thanh
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/karaoke-vi/">                                            Karaoke                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/micro/">                                            Micro                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/loa-di-dong/">                                            Loa Bluetooth                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dan-may-nghe-nhac/">                                            Dàn máy nghe nhạc                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/loa-soundbar/">                                            Loa Soundbar                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dau-dia-dvd-bluray/">                                            Đầu đĩa DVD Bluray                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/loa/">                                            Loa                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/amply/">                                            Amply                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-radio-cassette/">                                            Máy radio, Cassette                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1uZ2hlLW5oYWMtbXAzLw==">
                                                  Máy nghe nhạc MP3
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/day-cap-chuyen-doi/">                                            Dây cáp chuyển đổi                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2dpYS10cmVvLWxvYS8=">
                                                  Giá treo loa
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li className>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-giadung" /></div>
                                    <p><a href="/gia-dung/">Đồ gia dụng<img src="https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T07/bannerTLV/sticker_hot%402x.png" style={{maxHeight: 16, paddingLeft: 8, position: 'absolute'}} /></a></p>
                                  </div>
                                  <div className="sub-menu gia-dung" style={{display: 'none'}}>
                                    <div className=" menu-giadung children_sort" style={{display: 'inline-block'}}>
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L2dpYS1kdW5nLw==">Gia dụng nhà bếp
                                              <span className=" nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2JpbmgtdGh1eS1kaWVuLw==">
                                                  Bình thủy điện
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L25vaS1jb20tZGllbi8=">
                                                  Nồi cơm điện
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2JlcC1kaWVuLz9zdWJjYXRzPVkmZmVhdHVyZXNfaGFzaD0xNjgtMzUwNjkv">
                                                  Bếp từ
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" data-url="L2JlcC1kaWVuLz9zdWJjYXRzPVkmZmVhdHVyZXNfaGFzaD0xNjgtMzQ5MDQ=">
                                                  Bếp hồng ngoại
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2JlcC1nYXMv">
                                                  Bếp gas
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2xvLXZpLXNvbmctbWljcm93YXZlLw==">
                                                  Lò vi sóng
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2xvLW51b25nLXZpLW51b25nLw==">
                                                  Lò nướng
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2xhdS1kaWVuLWRhLW5hbmcv">
                                                  Lẩu điện
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" data-url="L25vaS1hcC1zdWF0Lw==">
                                                  Nồi áp suất
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L25vaS1kYS1uYW5nLw==">
                                                  Nồi chức năng
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2FtLW51b2MtYmluaC1udW9jLw==">
                                                  Bình đun siêu tốc
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1ydWEtY2hlbi8=">
                                                  Máy rửa chén
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            Thiết bị gia đình
                                          </h3>
                                          <ul>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1udW9jLW5vbmcv">
                                                  Máy nước nóng
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1odXQtYnVpLw==">
                                                  Máy hút bụi
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1sb2MtbnVvYy1ub25nLWxhbmgv">
                                                  Máy lọc nước nóng lạnh
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2JpbmgtbG9jLW51b2Mv">
                                                  Máy lọc nước
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1udW9jLW5vbmctbGFuaC8=">
                                                  Máy nước nóng lạnh
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2Jhbi11aS1iYW4tbGEv">
                                                  Bàn ủi
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3F1YXQtcGh1bi1zdW9uZy1xdWF0LWhvaS1udW9jLw==">
                                                  Quạt phun sương
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3F1YXQtZGllbi1xdWF0LW1heS8=">
                                                  Quạt điện - Quạt máy
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/quat-dieu-hoa-vi/">                                            Quạt điều hòa                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L29uLWFwLw==">
                                                  Ổn áp
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1sb2Mta2hvbmcta2hpLw==">
                                                  Máy lọc không khí
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1odXQtYW0v">
                                                  Máy hút ẩm, tạo ẩm
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            Máy xay, vắt, ép
                                          </h3>
                                          <ul>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS14YXktc2luaC10by8=">
                                                  Máy xay sinh tố
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1lcC10cmFpLWNheS8=">
                                                  Máy ép trái cây
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS12YXQtY2FtLw==">
                                                  Máy vắt cam
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1waGEtY2EtcGhlLw==">
                                                  Máy pha cà phê
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1sYW0tc3VhLWNodWEv">
                                                  Máy làm sữa chua
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS14YXktZGF1LW5hbmgv">
                                                  Máy xay đậu nành
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1kYW5oLXRydW5nLw==">
                                                  Máy đánh trứng
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS14YXktdGhpdC8=">
                                                  Máy xay thịt
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1sYW0tYmFuaC1taS8=">
                                                  Máy làm bánh mì
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L2dpYS1kdW5nLw==">Dụng cụ nhà bếp
                                              <span className=" nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2JvLW5vaS1uYXUtYW4v">
                                                  Xoong, nồi
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2NoYW8tY2hvbmctZGluaC8=">
                                                  Chảo chống dính
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3RvLWNoZW4tZGlhLw==">
                                                  Tô, Chén, Dĩa
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2R1bmctY3UtbmhhLWJlcC8=">
                                                  Dụng cụ ăn
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            Đồ dùng gia đình
                                          </h3>
                                          <ul>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2Rlbi1iYXQtbXVvaS8=">
                                                  Đèn bắt muỗi
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3ZvdC1tdW9pLw==">
                                                  Vợt muỗi
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L28tY2FtLWRpZW4v">
                                                  Ổ cắm điện
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2Rlbi1waW4v">
                                                  Đèn pin
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2JpbmgtbHVvbmctdGluaC8=">
                                                  Bình lưỡng tính
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-tulanh" /></div>
                                    <p><a href="/tu-lanh/">Tủ lạnh </a><a href="/tu-dong/">Tủ đông</a><a href="tu-mat-tu-giu-lanh/">Tủ mát</a></p>
                                  </div>
                                  <div className="sub-menu tu-lanh-tu-dong" style={{display: 'none'}}>
                                    <div className=" menu-tulanh children_sort">
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/tu-lanh/">Tủ lạnh
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh-toshiba/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Toshiba                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh-hitachi/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Hitachi                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh-sharp/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sharp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh-lg/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  LG                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" data-url="L3R1LWxhbmgtcGFuYXNvbmljLw==">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Panasonic
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh-electrolux/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Electrolux                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh-aqua/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  AQUA                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="https://www.nguyenkim.com/tu-lanh/?sort_by=position&sort_order=desc&features_hash=99999-3">                                            Tủ lạnh đời mới 2018                                                <span className="nki-sticker-new nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh/?subcats=Y&features_hash=177-6726">                                            Tủ lạnh Inverter                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh/?subcats=Y&features_hash=86-150-300-LIT">                                            Tủ lạnh 150-300 lít                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh/?subcats=Y&features_hash=34-0-4999999-VND">                                            Tủ lạnh dưới 5 triệu                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-lanh/?subcats=Y&features_hash=82-5872">                                            Tủ lạnh cao cấp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/tu-dong/">Tủ đông
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-dong-alaska/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Alaska                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tu-dong-sanaky/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sanaky                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/tu-mat-tu-giu-lanh/">Tủ mát
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/tu-mat-alaska/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Alaska                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/tu-mat-sanaky/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sanaky                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-maygiat" /></div>
                                    <p><a href="/may-giat/">Máy giặt </a><a href="/may-say-quan-ao/">Máy sấy</a></p>
                                  </div>
                                  <div className="sub-menu may-giat-may-say" style={{display: 'none'}}>
                                    <div className=" menu-maygiat children_sort">
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/may-giat/">Máy giặt
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat-lg/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  LG                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat-aqua/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  AQUA                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat-electrolux/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Electrolux                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" data-url="L21heS1naWF0LXRvc2hpYmEv">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Toshiba
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat-samsung/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Samsung                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat/?subcats=Y&features_hash=99999-2">                                            Máy giặt trả góp 0%                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat/?subcats=Y&features_hash=34-0-4999999-VND">                                            Máy giặt dưới 5 triệu                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat/?subcats=Y&features_hash=210-33524">                                            Máy giặt Inverter                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat/?subcats=Y&features_hash=72-5708">                                            Máy giặt cửa trên                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-giat/?subcats=Y&features_hash=72-6127">                                            Máy giặt cửa trước                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/may-giat/?features_hash=34-25000001-10000000000-VND">                                            Máy giặt cao cấp                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/may-say-quan-ao/">Máy sấy
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-say-quan-ao-electrolux/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Electrolux                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-say-quan-ao-candy/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Candy                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-say-quan-ao-whirlpool/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Whirlpool                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/may-say-quan-ao/?features_hash=74-86281-12812-9051-41686-10042-80406">                                            Máy sấy 8-10kg                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-say-quan-ao/?features_hash=74-27815-88901-10220">                                            Máy sấy trên 10kg                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-say-quan-ao/?features_hash=34-0-4999999-VND_34-5000000-7000000-VND_34-7000000-10000000-VND">                                            Mấy sấy dưới 10 triệu                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="https://www.nguyenkim.com/may-say-quan-ao/?features_hash=34-10000001-1000000000-VND">                                            Máy sấy trên 10 triệu                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-laptop" /></div>
                                    <p><a href="/may-tinh-xach-tay/">Laptop</a><a href="/may-tinh-de-ban/">PC</a><a href="/phu-kien-tin-hoc/">Phụ kiện</a></p>
                                  </div>
                                  <div className="sub-menu laptop" style={{display: 'none'}}>
                                    <div className=" menu-maytinh children_sort">
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/may-tinh-xach-tay/">Laptop
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/laptop-dell/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  DELL                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/laptop-apple-macbook/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Apple                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/laptop-acer/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Acer                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/laptop-asus/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  ASUS                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/laptop-hp/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  HP                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/laptop-lenovo/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Lenovo                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-xach-tay/?features_hash=367-97743">                                            Chơi game                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-xach-tay/?features_hash=371-97747">                                            Đồ họa                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-xach-tay/?features_hash=369-97745">                                            Thiết kế mỏng nhẹ                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-xach-tay/?features_hash=373-97749">                                            Học tập - văn phòng                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-xach-tay/?features_hash=34-0-4999999-VND_34-7000000-10000000-VND_34-5000000-7000000-VND">                                            Laptop dưới 10 triệu                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/may-tinh-de-ban/">Máy tính bàn
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-de-ban-apple-imac/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Apple                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-de-ban-dell/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  DELL                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-de-ban-acer/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Acer                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-de-ban-asus/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  ASUS                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-de-ban-hp/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  HP                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tinh-de-ban-lenovo/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Lenovo                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/man-hinh-lcd-vi-tinh/">Màn hình LCD
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/lcd-vi-tinh-dell/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  DELL                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/lcd-vi-tinh-hp/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  HP                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/asus/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  ASUS                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/lcd-vi-tinh-lg/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  LG                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/lcd-vi-tinh-samsung/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Samsung                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/phu-kien-tin-hoc/">Phụ kiện máy tính
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dan-loa-vi-tinh/">                                            Loa vi tính                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/chuot-may-tinh/">                                            Chuột máy tính                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/ban-phim/">                                            Bàn phím                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3VzYi12aS8=">
                                                  USB
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L28tY3VuZy1kaS1kb25nLw==">
                                                  Ổ cứng di động
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/tai-nghe-vi-tinh/">                                            Tai nghe vi tính                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/de-tan-nhiet/">                                            Đế tản nhiệt                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3RoaWV0LWJpLW1hbmcv">
                                                  Thiết bị mạng
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/phan-mem/">Phần mềm
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/microsoft-office/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Microsoft Office                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/microsoft-windows/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Microsoft Windows                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-mayanh" /></div>
                                    <p><a href="/ky-thuat-so/">Máy ảnh</a><a href="/may-quay-phim/">Máy Quay</a><a href="/phu-kien-ky-thuat-so/">Phụ kiện</a></p>
                                  </div>
                                  <div className="sub-menu may-anh" style={{display: 'none'}}>
                                    <div className=" menu-mayanh children_sort">
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/ky-thuat-so/">Máy ảnh
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-anh-mirrorless-canon/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Canon                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-anh-mirrorless-sony/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sony                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-anh-chuyen-nghiep-nikon/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Nikon                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-anh-mirrorless-fujifilm/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Fujifim                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-anh-mirrorless/?subcats=Y&features_hash=99999-2">                                            Máy ảnh trả góp 0%                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-anh-mirrorless/">                                            Máy ảnh Mirrorless                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-anh-chuyen-nghiep/">                                            Máy ảnh DSLR                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-anh-ky-thuat-so/">                                            Máy ảnh du lịch                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1jaHVwLWFuaC1sYXktbGllbi8=">
                                                  Máy ảnh chụp lấy liền
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/phu-kien-ky-thuat-so/">Phụ kiện máy ảnh
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/ong-kinh-may-anh/">                                            Ống kính                                                 <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/film-may-anh/">                                            Film máy ảnh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/chan-may-anh/">                                            Chân máy ảnh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3RoZS1uaG8v">
                                                  Thẻ nhớ
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2RhdS1kb2MtdGhlLW5oby8=">
                                                  Đầu đọc thẻ nhớ
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/may-quay-phim/">Máy quay phim
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-quay-phim-kts-gopro/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Go Pro                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1xdWF5LXBoaW0ta3RzLWNhbm9uLw==">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Canon
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-quay-phim-kts-sony/">                                                <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Sony                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/thiet-bi-giai-tri/">Thiết bị giải trí
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/playstation/">                                            Playstation                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/kinh-thuc-te-ao/">                                            Kính thực tế ảo                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2RpYS1nYW1lLw==">
                                                  Đĩa game
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/dong-ho-thoi-trang/">Đồng hồ
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="https://www.nguyenkim.com/dong-ho-thong-minh-apple/">                                            Apple Watch                                                <span className="nki-sticker-new nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/dong-ho-thong-minh-vi/">                                            Đồng hồ thông minh                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" rel="nofollow" href="/dong-ho-thoi-trang/">                                            Đồng hồ thời trang                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-mayin" /></div>
                                    <p><a href="/may-in-van-phong/">Máy in</a><a href="/thiet-bi-van-phong/">Thiết bị văn phòng</a></p>
                                  </div>
                                  <div className="sub-menu thiet-bi-van-phong" style={{display: 'none'}}>
                                    <div className=" menu-mayin children_sort">
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L21heS1pbi12YW4tcGhvbmcv">Máy in
                                              <span className=" nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi1jYW5vbi8=">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Canon
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi1icm90aGVyLw==">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Brother
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi1lcHNvbi8=">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Epson
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi1ocC8=">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  HP
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi1zYW1zdW5nLw==">
                                                  <span className="nki-sort-next ">
                                                    <span className="path1" /><span className="path2" />
                                                  </span>
                                                  Samsung
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi12YW4tcGhvbmcvP3N1YmNhdHM9WSZmZWF0dXJlc19oYXNoPTk5OTk5LTI=">
                                                  Máy in trả góp 0%
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi12YW4tcGhvbmcvP2ZlYXR1cmVzX2hhc2g9NTMtOTE2My05MjEyLTkyMTI=">
                                                  Máy in laser
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi12YW4tcGhvbmcvP2ZlYXR1cmVzX2hhc2g9NTMtMjkzMDctNTE4Ni05MjcxMw==">
                                                  Máy in phun
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi12YW4tcGhvbmcvP3N1YmNhdHM9WSZmZWF0dXJlc19oYXNoPTE5OC05Mjkw">
                                                  Máy in 2 mặt tự động
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi12YW4tcGhvbmcvP2ZlYXR1cmVzX2hhc2g9MzYzLTM5MzI3LTM5MzI1LTM5MzI5LTkyMDItODgwODktMjAzNzA=">
                                                  Máy in đa chức năng
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi12YW4tcGhvbmcvP2ZlYXR1cmVzX2hhc2g9NTMtOTc0NA==">
                                                  Máy in kim
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi12YW4tcGhvbmcvP3N1YmNhdHM9WSZmZWF0dXJlc19oYXNoPTM0LTAtNDk5OTk5OS1WTkQ=">
                                                  Máy in dưới 5 triệu
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L3RoaWV0LWJpLXZhbi1waG9uZy8=">Thiết bị văn phòng
                                              <span className=" nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1mYXgtZGEtbmFuZy8=">
                                                  Máy fax
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1waG90b2NvcHkv">
                                                  Máy photocopy
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1jaGlldS8=">
                                                  Máy chiếu
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1kZW0tdGllbi8=">
                                                  Máy đếm tiền
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS10aW5oLWJvLXR1aS8=">
                                                  Máy tính 
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1zY2FuLw==">
                                                  Máy scan
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1pbi1iaWxsLw==">
                                                  Máy in bill
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L3R1LWRpZW4tZGllbi10dS8=">
                                                  Kim từ điển
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1xdWV0LWJhcmNvZGUv">
                                                  Máy quét mã vạch
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1odXktZ2lheS8=">
                                                  Máy hủy giấy
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2JvLWx1dS1kaWVuLXVwcy8=">
                                                  Bộ lưu điện (UPS)
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1naGktYW0v">
                                                  Máy ghi âm
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L3BodS1raWVuLXZhbi1waG9uZy8=">Phụ kiện văn phòng
                                              <span className=" nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L211Yy1pbi8=">
                                                  Mực in
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21hbi1jaGlldS8=">
                                                  Màn chiếu
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-congcu" /></div>
                                    <p><a href="/dien-co/">Công cụ</a><a href="/bach-hoa/">Bách hóa</a><a href="/suc-khoe-va-lam-dep/">Làm đẹp</a></p>
                                  </div>
                                  <div className="sub-menu cong-cu" style={{display: 'none'}}>
                                    <div className=" menu-congcu children_sort">
                                      <div className="item row1 bg-white">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/dien-co/">Công cụ - Dụng cụ
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-khoan/">                                            Máy khoan                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-cua-may-mai/">                                            Máy mài                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-cat-gach/">                                            Máy cắt gạch                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-han-dien/">                                            Máy hàn điện                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-bom-nuoc/">                                            Máy bơm nước                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-phat-dien/">                                            Máy phát điện                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21heS1waHVuLXhpdC1ydWEv">
                                                  Máy phun xịt rửa
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/thang-nhom/">                                            Thang nhôm                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-may/">                                            Máy may                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/ket-sat/">                                            Két sắt                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/bach-hoa/">Bách hóa
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/san-pham-moi-hoa-my-pham-bot-giat/">                                            Bột giặt                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/nuoc-xa-vai/">                                            Nước xả vải                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/hoa-pham/">                                            Hóa mỹ phẩm                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/khan-long/">                                            Khăn                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dung-dich-tay-rua/">                                            Dung dịch tẩy rửa                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/nuoc-giai-khat/">                                            Thức uống                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/thung-rac/">                                            Bộ lau nhà, Thùng rác                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links">                                            Thảm lót sàn                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row2 bg-white">
                                        <div className="links">
                                          <h3>
                                            Phòng ngủ
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/nem/">                                            Nệm                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/bo-dra-trai-giuong-va-ao-goi/">                                            Drap                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/goi/">                                            Gối, Áo gối                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L21lbi8=">
                                                  Chăn - Mền
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                        <div className="links">
                                          <h3>
                                            <span className="js_hidden_link" data-url="L2JhbG8tdmEtdmFsaS8=">Balo, Vali
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </span>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/ba-lo/">                                            Balo                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                      <div className="item row1 bg-gray">
                                        <div className="links">
                                          <h3>
                                            <a className="a-links" href="/suc-khoe-va-lam-dep/">Làm đẹp
                                              <span className="NULL nk-sticker">
                                                <span className="path1" /><span className="path2" />
                                              </span>
                                            </a>
                                          </h3>
                                          <ul>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-say-toc/">                                            Máy sấy tóc, tạo kiểu tóc                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-cao-rau/">                                            Máy cạo râu                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <span className="js_hidden_link" rel="nofollow" data-url="L2Jhbi1jaGFpLWRhbmgtcmFuZy8=">
                                                  Bàn chải đánh răng
                                                  <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </span>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-massage-ghe-massage/">                                            Máy massage, ghế massage                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/may-tap-da-nang/">                                            Máy tập thể dục                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/can-suc-khoe/">                                            Cân sức khỏe                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/thiet-bi-y-te/">                                            Thiết bị y tế                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                            <li>
                                              <p style={{}}>
                                                <a className="a-links" href="/dong-ho-thoi-trang/">                                            Đồng hồ đeo tay                                                <span className="NULL nk-sticker">
                                                    <span className="path1" />
                                                    <span className="path2" />
                                                  </span>
                                                </a>
                                              </p>
                                            </li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="menu-item">
                                    <div className="icon"><i className="nk-sprite-desktop icon-simso" /></div>
                                    <p><a href="/sim-so-dep.html">Sim số - Thẻ cào - Thu hộ</a></p>
                                  </div>
                                  <div className="sub-menu sim-so" style={{display: 'none'}}>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div className="span12 ">
                            <div className=" menu-ngang">
                              <div id="nk-danh-muc-san-pham-right">
                                <div id="nk-khuyen-mai-hot">
                                  <div className=" menu-ngang-sub" style={{display: 'block'}}>
                                    <ul id="text_links_10024" className="ty-text-links">
                                      <li className="ty-text-links__item ty-level-0 nk-menu-gift2">
                                        <a className="ty-text-links__a" href="/mua-online-uu-dai-hon.html">Mua online ưu đãi hơn</a>
                                      </li>
                                      <li className="ty-text-links__item ty-level-0 nk-menu-hot">
                                        <a className="ty-text-links__a" href="/khuyen-mai.html">Khuyến mãi HOT</a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                <div id="nk-goc-cong-nghe" style={{display: 'block'}}>
                                  <div className="icon"><i className="nk-sprite-desktop icon-goc-cong-nghe" /></div>
                                  <a href="https://www.nguyenkim.com/blog.html"><span>Góc công nghệ</span></a>
                                </div>
                                <div id="nk-header-tra-gop-0" style={{display: 'block'}}><a href="https://www.nguyenkim.com/khuyen-mai-tra-gop-tai-nguyen-kim.html">Trả góp 0%</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

export default Header;