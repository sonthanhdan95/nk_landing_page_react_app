import React from 'react';
import './index.sass';

class NavMenu extends React.Component {
    constructor(props){
        super(props);
        console.log('NavMenu this.props.dataProps ',this.props.dataProps);
    }
    render(){
      const { styles,menuItems } = this.props.dataProps;
        const MenuItems = menuItems.map((menu,index)=>{
            return (
              <li key={index}
                className={"item-menu-"+menuItems.length}
                style={{ backgroundColor: styles.backgroundColorItem }}
              >
                <a
                  href={menu.linkTo}
                  data-scroll="d-list-products-1"
                  style={{ 
                    color: styles.colorTextItem, 
                    fontSize: 20 
                  }}
                >
                  {menu.item}
                </a>
              </li>
            );
        });
        return(
          <div className="container">
          <div className="lp-menu menu-theme1">
            <ul
              className="clearfix"
              style={{
                background: "transparent",
                position: "relative",
                top: "auto",
                padding: 0,
                zIndex: 999
              }}
            >
              {MenuItems}
            </ul>
          </div>
        </div>
        
        );
    }
}

export default NavMenu;