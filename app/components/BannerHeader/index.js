import React from 'react';
import './index.sass';

class BannerHeader extends React.Component {
    constructor(props){
        super(props);
        console.log('BannerHeader this.props.dataProps ',this.props.dataProps);
    }
    render() {
        return(
            <div className="banner-1Col">
                <img src={this.props.dataProps.dataSrcD} alt={this.props.dataProps.dataAltD}/>
            </div>
        );
    }
}
export default BannerHeader;
