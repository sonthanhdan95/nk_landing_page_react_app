import React from 'react';
import './index.sass';
import CountDownTimer from '../CountDownTimer'

class CountDownCoupon extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            couponTotal: 100,
            couponNumber : 50,
            progressBar: 100
        }
        console.log('CountDownCoupon this.props.dataProps',this.props.dataProps);
    }
    render(){
        const { dataSrc,couponTitle } = this.props.dataProps;
        const {couponNumber, progressBar,couponTotal} = this.state;
        const currentDate = new Date();
        const year = (currentDate.getMonth() === 11 && currentDate.getDate() > 23) ? currentDate.getFullYear() + 1 : currentDate.getFullYear();
        var someDate = new Date();
        var numberOfDaysToAdd = new Date('2018-12-03T23:59:34.606Z').getDate()
        someDate.setDate(someDate.getDate() + numberOfDaysToAdd); 
        return(
            <div >
                <CountDownTimer date={`${someDate}`} />
            <div
              className="bg-countd"
              style={{
                background:
                  'url("'+ dataSrc +'") center center no-repeat',
                marginTop: 20
              }}
            >
              <div className="countd-inner">
                <img src="https://cdn.nguyenkimmall.com/images/companies/_1/html/2018/T10/countdown/stopwatch.png" />
                <div className="text-countd">
                  <h2>
                    {couponTitle}
                    <span className="brand-countd" />
                    <span className="text1">Số lượng</span>
                    <span className="sale-countd">{couponTotal}</span>
                  </h2>
                  <div className="time-countd">
                    <div className="level" style={{ width: 50*690/100 }} />
                    <h3>Chỉ còn {couponNumber} mã giảm giá</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        );
    }
}

export default CountDownCoupon;