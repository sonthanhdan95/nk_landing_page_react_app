import React from 'react'

class NavTitle extends React.Component {
    constructor(props){
        super(props);
      
    }
    render(){
        console.log('NavTitle this.props.dataProps',this.props.dataProps);
        const {content,backgroundColor,fontSize } = this.props.dataProps;
        return(
            <div className="container">
                <div className="lp-products">
                    <div className="box" id="d-list-products-1">
                    <div className="title" style={{ background: backgroundColor }}>
                        <h2
                        className="head2"
                        style={{ padding: 10, color: "rgb(0, 0, 0)", fontSize: 20 }}
                        >
                        {content}
                        </h2>
                    </div>
                    </div>
                    <div className="lp-banner-promotion-key clearfix desktop1items1rows1" />
                </div>
            </div>
        );
    }
}

export default NavTitle;