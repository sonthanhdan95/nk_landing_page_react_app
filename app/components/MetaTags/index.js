import React from 'react';
import { Helmet } from "react-helmet";

class MetaTags extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  };
        console.log('MetaTags this.props.dataProps ',this.props.dataProps);
    }
    render() {
        const { landing_page_title, 
                meta_description, 
                meta_keywords,
                seo_name
            } = this.props.dataProps
        return (
            <Helmet>
                <title>{landing_page_title}</title>
                <base href="https://www.nguyenkim.com/" />
                <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="revisit-after" content="1 days" />
                <meta property="og:locale" content="vi_VN" />
                <meta
                    name="description"
                    content={meta_description}
                />
                <meta
                    name="keywords"
                    content={meta_keywords}
                />
                <meta name="robots" content="INDEX,FOLLOW" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
                <meta
                    name="google-translate-customization"
                    content="16fd9cdc8e5dff68-5b4f5d3241f56289-g7990b2bb092cc2f6-11"
                />
                <link rel="author" href="https://plus.google.com/+nguyenkim/posts" />
                <link rel="publisher" href="https://plus.google.com/+nguyenkim" />
                <meta property="og:type" content="article" />
                <meta
                    property="og:title"
                    content={landing_page_title}
                />
                <meta
                    property="og:site_name"
                    content="Siêu thị điện máy Nguyễn Kim - www.nguyenkim.com"
                />
                <meta property="fb:app_id" content={534767553533391} />
                <meta property="fb:pages" content={370632339670121} />
                <meta
                    property="og:image"
                    content="https://cdn.nguyenkimmall.com/images/detailed/557/fbads-bb-tdc1.png"
                />
                <meta
                    property="og:url"
                    content={"https://www.nguyenkim.com/"+seo_name}
                />
                <meta
                    property="og:description"
                    content={meta_description}
                />
                <meta
                    itemProp="name"
                    content={landing_page_title}
                />
                <meta
                    itemProp="description"
                    content={meta_description}
                />
                <meta
                    itemProp="image"
                    content="https://cdn.nguyenkimmall.com/images/detailed/557/fbads-bb-tdc1.png"
                />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:site" content="@Nguyenkimonline" />
                <meta
                    name="twitter:title"
                    content={landing_page_title}
                />
                <meta
                    name="twitter:description"
                    content={meta_description}
                />
                <meta name="twitter:creator" content="@nguyenkimonline" />
                <meta
                    name="twitter:image:src"
                    content="https://cdn.nguyenkimmall.com/images/detailed/557/fbads-bb-tdc1.png"
                />
                <meta property="fb:pages" content={150921051593902} />
                <meta name="format-detection" content="telephone=no" />
                <link
                    href="https://cdn.nguyenkimmall.com/images/favicon/icon803.png"
                    rel="shortcut icon"
                />
            </Helmet>
        );
    }
}

export default MetaTags;