import React from 'react';

class LabelTraGop0 extends React.Component {
    constructor(props){
        super(props);

    }
    render(){
        if(this.props.dataProps == 1){
            return(
                <span className="nk_tragop_0">
                    <img
                        src="https://cdn.nguyenkimmall.com/images/companies/_1/Data_Price/Pic_Tags/tap-tragop0dong.png"
                        alt
                    />
                </span>
            );
        } else {
            return(
                <span className="nk_tragop_0"></span>
            );
        }
    }
}

export default LabelTraGop0;