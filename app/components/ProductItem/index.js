import React from 'react'
import './index.sass';
import LabelTraGop0 from './LabelTraGop0.js';
import { FORMAT_CURRENCY } from '../Helpers';
import axios from 'axios';
class ProductItem extends React.Component {
    constructor(props){
      super(props);

    }
    handleSubmit(){
      const config = { 
        headers: { 
          'Content-Type': 'multipart/form-data' 
        } 
      };
      axios.post('', data, config)
      .then(response => console.log(response))
      .catch(errors => console.log(errors));
    }
    render(){
      const product = this.props.dataProps;
      console.log('ProductItem product',product);
        return(
          <div className="container">
            <div id={product.product_id} className="item nk-fgp-items">
              <a
                className="nk-link-product"
                href={"https://www.nguyenkim.com/"+product.seo_name+".html"}
              >
                <div className="nk-product-img margbt20">
                  <img
                    className="lazyload"
                    src={product.images}
                    alt={product.product_name}
                    title={product.product_name}
                    style={{}}
                  />
                </div>
                <div className="nk-product-desc">
                  <div className="title">
                    <strong className="brand">{product.brand}</strong>
                    <LabelTraGop0 dataProps={product.nk_tragop_0}/>
                  </div>
                  <div className="label">
                    {product.product_name}
                  </div>
                  <div className="desc" dangerouslySetInnerHTML={{ __html: product.short_description }}></div>
                  <p className="price" />
                  <p className="price discount">
                    {FORMAT_CURRENCY(product.price)}
                  </p>
                  <span className="promotion margbt10">
                    {product.text_promotion}
                  </span>
                </div>
              </a>
              <div className="nk-product-hover">
                <form
                  action="https://www.nguyenkim.com/"
                  method="post"
                  name={"product_form_"+product.product_id}
                  encType="multipart/form-data"
                  className="cm-processed-form"
                >
                  <input
                    type="hidden"
                    name={"product_data["+ product.product_id +"][product_id]"}
                    defaultValue={product.product_id}
                  />
                  <input
                    type="hidden"
                    name={"product_data["+ product.product_id +"][amount]"}
                    defaultValue={1}
                  />
                  <input
                    type="submit"
                    style={{ display: "none" }}
                    name={"dispatch[checkout.add2.."+ product.product_id +"]"}
                    id={"button_cart_"+product.product_id}
                  />
                  <div className="nk-btn">
                    <a
                      id={"btn-buy-"+product.product_id}
                      style={{}}
                      href="javascript:void(0)"
                      className="nk-btn-buy"
                    >
                      Mua ngay
                    </a>
                    <a
                    //   id="btn-comming-off-72443"
                      style={{ display: "none" }}
                      href="javascript:void(0)"
                      className="nk-btn-comming"
                    >
                      Sắp có hàng về
                    </a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        );
    }
}

export default ProductItem;