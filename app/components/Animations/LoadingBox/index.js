import React from 'react'
import Header from '../../Header';
import Footer from '../../Footer';
import './index.sass';

class LoadingBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            displayType: 'none'
         };
    }
    componentWillMount() {
        const { isShow} = this.props;
        if(isShow){
            this.setState({
                displayType: 'block'
            });
        }
    }
    render() {
        return (
            <div className="ty-helper-container no-touch" id="tygh_main_container">
                <Header/>
                <div style={{ display: this.state.displayType }} id="ajax_loading_box" className="ty-ajax-loading-box"></div>
                <Footer/>
            </div>
            
        );
    }
}

export default LoadingBox;