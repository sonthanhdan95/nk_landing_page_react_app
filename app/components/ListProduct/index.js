import React from 'react'
import axios from 'axios';
import ProductItem from '../ProductItem';
import { 
    URL_NK_API_ENDPOINT,
    Authorization_API,
    Token_Pass_API
} from '../../constant/endpoint_constant';

class ListProduct extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading : false,
            products : []
        };

        this.fetchListProduct.bind(this)
        // this.SeenProducts.bind(this)
    }
    // SeenProducts() {
    //     return fetch("https://dev.api.nguyenkimonline.com/v1/GetListProduct", {
    //       method: "POST",
    //       headers: {
    //         'crossDomain': true,
    //         'Access-Control-Allow-Origin': '*',
    //         'Authorization': `${Authorization_API}`,
    //         'password': `${Token_Pass_API}`,
    //         'Content-Type': 'application/json'
    //       },
    //       body: JSON.stringify({
    //         products_list: [63835, 63825, 63793]
    //       })
    //     })
    //     .then(res => res.json())
    //     .then(resjson => resjson.data);
    // }
    fetchListProduct(productIds){
        const self = this;
        axios.get(`https://beta.api.nguyenkimonline.com/v1/GetListProduct1`,{
            headers: {
                'crossDomain': true,
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            data: {
                products_list : productIds
            },
        }).then((response) => {
            self.setState({
                isLoading: true,
                products: response.data.data
            });
        }).catch(err => {
            throw err;
        });
    }
    componentWillMount(){
        const { productIds} = this.props.dataProps;
        this.fetchListProduct(productIds);
        // this.SeenProducts()
    }
    render(){
        console.log('ListProduct123 this.props.dataProps ',this.props.dataProps);
        const { productIds} = this.props.dataProps;
        // this.fetchListProduct(productId);
        console.log('Product123 this.props.dataProps ',productIds);
        console.log('ProductItem ',this.props.dataProps);
       

        const listProduct = this.state.products.map((product, index) =>
            <ProductItem key={index} dataProps={product}/>
        );
        return(
            <div className="lp-products">
            <div className="box" id="d-list-products-1">
                <div className="lp-banner-promotion-key clearfix desktop1items1rows1" />
                <div className="lp-products-list">
                <div className="nk-product-cate-style-grid nk-product- clearfix">
                    <div id="pagination_contents" className="nk-product">
                        {listProduct}
                    </div>
                </div>
                </div>
            </div>
            </div>
        );
    }

}

export default ListProduct;