import React from 'react';
import './index.sass';

class BannerColumn extends React.Component {
    constructor(props){
        super(props);
        console.log('BannerColumn this.props.dataProps ',this.props.dataProps);
    }
    render() {
        const { images,styleD } = this.props.dataProps;
        const ListImage = images.map((image,index)=>{
            switch(styleD){
                case '1Col':
                    return (<img key={index} src={image.dataSrcD} alt={image.dataAltD}/>);
                case '2Col':
                    return (<div key={index} className="item"><img src={image.dataSrcD} alt={image.dataAltD}/></div>);
                case '3Col':
                    return (<div key={index} className="item"><img src={image.dataSrcD} alt={image.dataAltD}/></div>);
                case 'Carousel':
                    return (<div key={index} className="carousel-item"><img src={this.props.dataProps.dataSrcD} alt={this.props.dataProps.dataAltD}/></div>);
                default:
            }  
        });

        return(
            <div className="container-fluid">
                <div className={"banner-"+styleD}>
                    {ListImage}
                </div>
            </div>
        );
    }
}
export default BannerColumn;
