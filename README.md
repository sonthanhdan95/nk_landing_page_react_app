# SETUP

1. Clone source 
```sh
$ git clone https://sonthanhdan95@bitbucket.org/sonthanhdan95/nk_landing_page_react_app.git
```
2. Install
```sh
$ cd nk_landing_page_react_app && npm install
$ npm i -g cross-env
$ npm i -g nodemon
```
alias command cross-env on your desktop 
[Read more](https://github.com/kentcdodds/cross-env#readme)

3. Run server
```sh
$ npm run dev:server
```
[http://localhost:3000](http://localhost:3000)

4. Run client
```sh
$ npm run dev:client
```
[http://localhost:8000](http://localhost:8000)

# DEMO
[demo_local](http://localhost:8000/mo-ban-iphone-xs-xsmax?src=data_mo-ban-iphone-xs-xsmax)


