const express = require("express");
const models  = require('./../../models');
const db      = require('../../lib/NkMySqlDB');
const api     = express.Router();
const co      = require('co');

api.get('/seo-landingpage/:seoname',function(req,res){
    const {seoname } = req.params;
    console.log(seoname);
    models.NkSeoLink.findOne({
        "url": seoname
    }).then(seo_link => {
        console.log(seo_link);
        res.json({
            "ldp_id": seo_link.object_id,
        })
    }).catch(err => {
        res.json({
            "ldp_id": 0,
        })
    });
});
api.get('/detail-landingpage/:ldp_id',function(req,res){
    const {ldp_id } = req.params;
    co(function *(){
        // resolve multiple promises in parallel
        var data1 = models.LandingPage.findOne({
            "landing_page_id": ldp_id
        }).then(landing_page => {
            return landing_page;
        }).catch(err => {
            console.log(err)
        });

        var data2 = models.LandingPageLayout.findOne({
            "landing_page_id": ldp_id
        }).then(layouts => {
            return layouts;
        }).catch(err => {
            console.log(err)
        });
        data2.then(function(data){
            console.log(data)
            data1.then(function(dataN){
                console.log(dataN)
                res.json({
                        landing_page: dataN,
                        layouts: data.layouts
                    });
            });
        });
      }).catch((err)=> console.log(err));
});

api.post('/get-list-products',function(req,res){

    var queryStr = `SELECT 
                        cscart_products.*,
                        cscart_product_descriptions.product,
                        MIN(
                        IF
                            ( cscart_product_prices.percentage_discount = 0, cscart_product_prices.price, cscart_product_prices.price - ( cscart_product_prices.price * cscart_product_prices.percentage_discount ) / 100 ) 
                        ) AS price,
                        GROUP_CONCAT( IF ( cscart_products_categories.link_type = 'M', CONCAT( cscart_products_categories.category_id, 'M' ), cscart_products_categories.category_id ) ) AS category_ids,
                        companies.company AS company_name,
                        cscart_supplier_links.supplier_id,
                        cscart_products.is_returnable,
                        cscart_products.return_period,
                        cscart_product_sales.amount AS sales_amount,
                        cscart_seo_names.NAME AS seo_name,
                        cscart_seo_names.path AS seo_path,
                        cscart_seo_names.master_url AS seo_master_url,
                        cscart_seo_names.name_master AS seo_name_master,
                        cscart_seo_names.is_default AS seo_is_default,
                        MIN( point_prices.point_price ) AS point_price,
                        cscart_discussion.type AS discussion_type 
                    FROM
                        cscart_products
                        LEFT JOIN cscart_product_prices ON cscart_product_prices.product_id = cscart_products.product_id 
                        AND cscart_product_prices.lower_limit = 1 
                        AND cscart_product_prices.usergroup_id IN ( 0, 0, 2 )
                        LEFT JOIN cscart_product_descriptions ON cscart_product_descriptions.product_id = cscart_products.product_id 
                        AND cscart_product_descriptions.lang_code = 'vi'
                        LEFT JOIN cscart_companies AS companies ON companies.company_id = cscart_products.company_id
                        INNER JOIN cscart_products_categories ON cscart_products_categories.product_id = cscart_products.product_id
                        INNER JOIN cscart_categories ON cscart_categories.category_id = cscart_products_categories.category_id 
                        AND ( cscart_categories.usergroup_ids = '' OR FIND_IN_SET( 0, cscart_categories.usergroup_ids ) OR FIND_IN_SET( 2, cscart_categories.usergroup_ids ) ) 
                        AND ( cscart_products.usergroup_ids = '' OR FIND_IN_SET( 0, cscart_products.usergroup_ids ) OR FIND_IN_SET( 2, cscart_products.usergroup_ids ) ) 
                        AND cscart_categories.STATUS IN ( 'A', 'H' ) 
                        AND cscart_products.STATUS IN ( 'A', 'H' )
                        LEFT JOIN cscart_supplier_links ON cscart_supplier_links.object_id = cscart_products.product_id 
                        AND cscart_supplier_links.object_type = 'P'
                        LEFT JOIN cscart_product_sales ON cscart_product_sales.product_id = cscart_products.product_id 
                        AND cscart_product_sales.category_id = 997
                        LEFT JOIN cscart_seo_names ON cscart_seo_names.object_id = cscart_products.product_id
                        AND cscart_seo_names.type = 'p' 
                        AND cscart_seo_names.dispatch = '' 
                        AND cscart_seo_names.lang_code = 'vi'
                        LEFT JOIN cscart_product_point_prices AS point_prices ON point_prices.product_id = cscart_products.product_id 
                        AND point_prices.lower_limit = 1 
                        AND point_prices.usergroup_id IN ( 0, 0, 2 )
                        LEFT JOIN cscart_discussion ON cscart_discussion.object_id = cscart_products.product_id 
                        AND cscart_discussion.object_type = 'P' 
                    WHERE
                        FIND_IN_SET( cscart_products.product_id, '67132,54578,55858,55988,56279,58755,67555' ) 
                        AND ( companies.STATUS = 'A' OR cscart_products.company_id = 0 ) 
                    GROUP BY
                        cscart_products.product_id`;

    db.query(queryStr,function(err,productsA){
        console.log(JSON.stringify(productsA));
        res.json({
            status: true,
            products: productsA
        });
    });

});




module.exports = api;