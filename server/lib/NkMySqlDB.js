const mysql       = require('mysql');
const config      = require('../config');
const connection  = mysql.createConnection({
    host        : config.NK_DB_HOST,
    user        : config.NK_DB_USER,
    password    : config.NK_DB_PASS,
    database    : config.NK_DB_NAME
});
connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;