const mongoose = require('mongoose');
const config   = require('../config'); 
require('dotenv').config();

exports.init = function(){
    return new Promise(function(resolve,reject){
        mongoose.Promise = global.Promise;
        const mongoConn = mongoose.connection;
        mongoose.connect(config.NK_MONGODB_URI,{
            useMongoClient: true
        });
        mongoConn.on('error',()=>reject(err));
        mongoConn.on('open',()=>resolve()); 
    });
}