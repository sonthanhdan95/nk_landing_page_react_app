const express = require('express');
const compression = require('compression');
const bodyParser = require('body-parser');
const http = require('http');
const path = require('path');
const morgan = require('morgan');
const debug = require('debug');
const info = debug('server:app:info');
const error = debug('server:app:error');
const databaseMongo = require('./lib/NkMongoDB');
const routes = require('./app');
var cors = require('cors');
const app = express();

const server = http.createServer(app);
app.disable('x-powered-by');
databaseMongo.init().then(()=>{info('connected to database');console.log('connected to database');}).catch((err)=>error(err));
const env = app.get('env');
if(env === 'production'){
    app.use(morgan('common',{
        stream: path.join(__dirname,'/../morgan.log')
    }));
} else {
    app.use(morgan('dev'));
}
app.use(compression());
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://dev.api.nguyenkimonline.com");
    // res.header("Access-Control-Allow-Headers", "*");
    next();
  });
app.use(cors(
    
));
routes(app);
process.on('uncaughtException',(err)=>{
    error('crashed!!! - ' + (err.stack || err));
});
const PORT = process.env.PORT || 3000;
server.listen(PORT, function(){
    console.log(`server is listening on ${PORT}`);
});