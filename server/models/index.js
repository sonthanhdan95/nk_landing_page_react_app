const LandingPage  = require('./LandingPage.js')
const LandingPageLayout = require('./LandingPageLayout.js');
const NkSeoLink = require('./NkSeoLink.js');


const db = {};
db.LandingPage = LandingPage;
db.LandingPageLayout = LandingPageLayout;
db.NkSeoLink = NkSeoLink;

module.exports = db;
