const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const landingPageSchema = new Schema({
    landing_page_id : {
      type: Number
    },
    name: {
      type: String
    },
    timestamp : {
      type : Number
    },
    start_time : {
      type: Number
    },
    end_time : {
      type : Number
    },
    status : {
      type: String
    },
    landing_page_title : {
      type: String
    },
    meta_description : {
      type : String
    },
    meta_keywords : {
      type: String
    },
    seo_name: {
      type: String
    },
    landing_page_image_data: {
      type: Array
    },
    styles: {
      type: Object
    }
  },{ 
    versionKey: false 
  });


module.exports = mongoose.model('landing_page',landingPageSchema,'nk_landing_pages');