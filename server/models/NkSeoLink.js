const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NkSeoLinkSchema = new Schema({
    object_id: {
        type: Number
    },
    type: {
        type: String
    },
    url: {
        type : String
    }
},{ 
    versionKey: false 
});


module.exports = mongoose.model('nk_seo_link',NkSeoLinkSchema,'nk_seo_links');