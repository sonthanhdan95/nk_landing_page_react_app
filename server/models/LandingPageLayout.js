const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const landingPageLayoutSchema = new Schema({
    landing_page_id : {
        type: Number
    },
    layouts :  {
        type : Array
    }
},{ 
    versionKey: false 
});

module.exports = mongoose.model('landing_page_layout',landingPageLayoutSchema,'nk_landing_page_layouts');